/*******************************************************************************
 *
 * Pentaho Data Integration
 *
 * Copyright (C) 2002-2012 by Pentaho : http://www.pentaho.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/

package org.pentaho.di.trans.steps.xmlinputstream;

import java.util.List;
import java.util.Map;

import org.pentaho.di.core.CheckResult;
import org.pentaho.di.core.CheckResultInterface;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.Counter;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMeta;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.core.variables.VariableSpace;
import org.pentaho.di.core.xml.XMLHandler;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;
import org.w3c.dom.Node;

/**
 * @author Jens Bleuel
 * @since 2011-01-13
 */
public class XMLInputStreamMeta extends BaseStepMeta implements StepMetaInterface
{
	private final static int DEFAULT_STRING_LEN_FILENAME = 256; // default length for XML path 

	private final static int DEFAULT_STRING_LEN_PATH = 1024; // default length for XML path

	public final static String DEFAULT_STRING_LEN = "1024"; // used by defaultStringLen

	public final static String DEFAULT_ENCODING = "UTF-8"; // used by encoding

	private String filename;

	private String xmlBlockTag;

	private boolean addResultFile;

	/** The number of rows to ignore before sending rows to the next step */
	private String nrRowsToSkip; //String for variable usage, enables chunk loading defined in an outer loop

	/** The maximum number of lines to read */
	private String rowLimit; //String for variable usage, enables chunk loading defined in an outer loop

	/** This is the default String length for name/value elements & attributes */
	private String defaultStringLen; // default set to DEFAULT_STRING_LEN

	/** Encoding to be used */
	private String encoding; // default set to DEFAULT_ENCODING

	/** Enable Namespaces in the output? (will be slower) */
	private boolean enableNamespaces;

	/** Trim all name/value elements & attributes? */
	private boolean enableTrim; // trim is also eliminating white spaces, tab, cr, lf at the beginning and end of the string

	// The fields in the output stream
	private boolean includeFilenameField;

	private String filenameField;

	private boolean includeRowNumberField;

	private String rowNumberField;

	private boolean includeXmlDataTypeNumericField;

	private String xmlDataTypeNumericField;

	private boolean includeXmlDataTypeDescriptionField;

	private String xmlDataTypeDescriptionField;

	private boolean includeXmlLocationLineField;

	private String xmlLocationLineField;

	private boolean includeXmlLocationColumnField;

	private String xmlLocationColumnField;

	private boolean includeXmlElementIDField;

	private String xmlElementIDField;

	private boolean includeXmlParentElementIDField;

	private String xmlParentElementIDField;

	private boolean includeXmlBlockIDField;

	private String xmlBlockIDField;

	private boolean includeXmlElementLevelField;

	private String xmlElementLevelField;

	private boolean includeXmlPathField;

	private String xmlPathField;

	private boolean includeXmlParentPathField;

	private String xmlParentPathField;

	private boolean includeXmlDataNameField;

	private String xmlDataNameField;

	private boolean includeXmlDataValueField;

	private String xmlDataValueField;

	public XMLInputStreamMeta()
	{
		super(); // allocate BaseStepMeta
	}

	@Override
	public void getFields(RowMetaInterface r, String name, RowMetaInterface[] info, StepMeta nextStep, VariableSpace space)
	{
		int defaultStringLenNameValueElements = Const.toInt(space.environmentSubstitute(this.defaultStringLen), new Integer(DEFAULT_STRING_LEN));

		if (this.includeFilenameField) {
			ValueMetaInterface v = new ValueMeta(space.environmentSubstitute(this.filenameField), ValueMeta.TYPE_STRING);
			v.setLength(DEFAULT_STRING_LEN_FILENAME);
			v.setOrigin(name);
			r.addValueMeta(v);
		}
		if (this.includeRowNumberField) {
			ValueMetaInterface v = new ValueMeta(space.environmentSubstitute(this.rowNumberField), ValueMeta.TYPE_INTEGER);
			v.setLength(ValueMetaInterface.DEFAULT_INTEGER_LENGTH);
			v.setOrigin(name);
			r.addValueMeta(v);
		}
		if (this.includeXmlDataTypeNumericField) {
			ValueMetaInterface vdtn = new ValueMeta(space.environmentSubstitute(this.xmlDataTypeNumericField), ValueMeta.TYPE_INTEGER);
			vdtn.setLength(ValueMetaInterface.DEFAULT_INTEGER_LENGTH);
			vdtn.setOrigin(name);
			r.addValueMeta(vdtn);
		}

		if (this.includeXmlDataTypeDescriptionField) {
			ValueMetaInterface vdtd = new ValueMeta(space.environmentSubstitute(this.xmlDataTypeDescriptionField), ValueMeta.TYPE_STRING);
			vdtd.setLength(25);
			vdtd.setOrigin(name);
			r.addValueMeta(vdtd);
		}

		if (this.includeXmlLocationLineField) {
			ValueMetaInterface vline = new ValueMeta(space.environmentSubstitute(this.xmlLocationLineField), ValueMeta.TYPE_INTEGER);
			vline.setLength(ValueMetaInterface.DEFAULT_INTEGER_LENGTH);
			vline.setOrigin(name);
			r.addValueMeta(vline);
		}

		if (this.includeXmlLocationColumnField) {
			ValueMetaInterface vcol = new ValueMeta(space.environmentSubstitute(this.xmlLocationColumnField), ValueMeta.TYPE_INTEGER);
			vcol.setLength(ValueMetaInterface.DEFAULT_INTEGER_LENGTH);
			vcol.setOrigin(name);
			r.addValueMeta(vcol);
		}

		if (this.includeXmlElementIDField) {
			ValueMetaInterface vdid = new ValueMeta(space.environmentSubstitute(this.xmlElementIDField), ValueMeta.TYPE_INTEGER);
			vdid.setLength(ValueMetaInterface.DEFAULT_INTEGER_LENGTH);
			vdid.setOrigin(name);
			r.addValueMeta(vdid);
		}

		if (this.includeXmlParentElementIDField) {
			ValueMetaInterface vdparentid = new ValueMeta(space.environmentSubstitute(this.xmlParentElementIDField), ValueMeta.TYPE_INTEGER);
			vdparentid.setLength(ValueMetaInterface.DEFAULT_INTEGER_LENGTH);
			vdparentid.setOrigin(name);
			r.addValueMeta(vdparentid);
		}

		if (this.includeXmlBlockIDField) {
			ValueMetaInterface vdblockid = new ValueMeta(space.environmentSubstitute(this.xmlBlockIDField), ValueMeta.TYPE_INTEGER);
			vdblockid.setLength(ValueMetaInterface.DEFAULT_INTEGER_LENGTH);
			vdblockid.setOrigin(name);
			r.addValueMeta(vdblockid);
		}

		if (this.includeXmlElementLevelField) {
			ValueMetaInterface vdlevel = new ValueMeta(space.environmentSubstitute(this.xmlElementLevelField), ValueMeta.TYPE_INTEGER);
			vdlevel.setLength(ValueMetaInterface.DEFAULT_INTEGER_LENGTH);
			vdlevel.setOrigin(name);
			r.addValueMeta(vdlevel);
		}

		if (this.includeXmlPathField) {
			ValueMetaInterface vdparentxp = new ValueMeta(space.environmentSubstitute(this.xmlPathField), ValueMeta.TYPE_STRING);
			vdparentxp.setLength(DEFAULT_STRING_LEN_PATH);
			vdparentxp.setOrigin(name);
			r.addValueMeta(vdparentxp);
		}

		if (this.includeXmlParentPathField) {
			ValueMetaInterface vdparentpxp = new ValueMeta(space.environmentSubstitute(this.xmlParentPathField), ValueMeta.TYPE_STRING);
			vdparentpxp.setLength(DEFAULT_STRING_LEN_PATH);
			vdparentpxp.setOrigin(name);
			r.addValueMeta(vdparentpxp);
		}

		if (this.includeXmlDataNameField) {
			ValueMetaInterface vdname = new ValueMeta(space.environmentSubstitute(this.xmlDataNameField), ValueMeta.TYPE_STRING);
			vdname.setLength(defaultStringLenNameValueElements);
			vdname.setOrigin(name);
			r.addValueMeta(vdname);
		}

		if (this.includeXmlDataValueField) {
			ValueMetaInterface vdval = new ValueMeta(space.environmentSubstitute(this.xmlDataValueField), ValueMeta.TYPE_STRING);
			vdval.setLength(defaultStringLenNameValueElements);
			vdval.setOrigin(name);
			r.addValueMeta(vdval);
		}

	}

	@Override
	public void loadXML(Node stepnode, List<DatabaseMeta> databases, Map<String, Counter> counters) throws KettleXMLException
	{
		try {
			this.filename = Const.NVL(XMLHandler.getTagValue(stepnode, "filename"), "");
			this.addResultFile = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "addResultFile"));

			this.nrRowsToSkip = Const.NVL(XMLHandler.getTagValue(stepnode, "nrRowsToSkip"), "0");
			this.rowLimit = Const.NVL(XMLHandler.getTagValue(stepnode, "rowLimit"), "0");
			this.defaultStringLen = Const.NVL(XMLHandler.getTagValue(stepnode, "defaultStringLen"), DEFAULT_STRING_LEN);
			this.encoding = Const.NVL(XMLHandler.getTagValue(stepnode, "encoding"), DEFAULT_ENCODING);
			this.enableNamespaces = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "enableNamespaces"));
			this.enableTrim = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "enableTrim"));

			// The fields in the output stream
			// When they are undefined (checked with NVL) the original default value will be taken
			this.includeFilenameField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeFilenameField"));
			this.filenameField = Const.NVL(XMLHandler.getTagValue(stepnode, "filenameField"), this.filenameField);

			this.includeRowNumberField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeRowNumberField"));
			this.rowNumberField = Const.NVL(XMLHandler.getTagValue(stepnode, "rowNumberField"), this.rowNumberField);

			this.includeXmlDataTypeNumericField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeDataTypeNumericField"));
			this.xmlDataTypeNumericField = Const.NVL(XMLHandler.getTagValue(stepnode, "dataTypeNumericField"), this.xmlDataTypeNumericField);

			this.includeXmlDataTypeDescriptionField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeDataTypeDescriptionField"));
			this.xmlDataTypeDescriptionField = Const.NVL(XMLHandler.getTagValue(stepnode, "dataTypeDescriptionField"), this.xmlDataTypeDescriptionField);

			this.includeXmlLocationLineField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeXmlLocationLineField"));
			this.xmlLocationLineField = Const.NVL(XMLHandler.getTagValue(stepnode, "xmlLocationLineField"), this.xmlLocationLineField);

			this.includeXmlLocationColumnField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeXmlLocationColumnField"));
			this.xmlLocationColumnField = Const.NVL(XMLHandler.getTagValue(stepnode, "xmlLocationColumnField"), this.xmlLocationColumnField);

			this.includeXmlElementIDField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeXmlElementIDField"));
			this.xmlElementIDField = Const.NVL(XMLHandler.getTagValue(stepnode, "xmlElementIDField"), this.xmlElementIDField);

			this.includeXmlParentElementIDField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeXmlParentElementIDField"));
			this.xmlParentElementIDField = Const.NVL(XMLHandler.getTagValue(stepnode, "xmlParentElementIDField"), this.xmlParentElementIDField);

			this.includeXmlBlockIDField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeXmlBlockIDField"));
			this.xmlBlockIDField = Const.NVL(XMLHandler.getTagValue(stepnode, "xmlBlockIDField"), this.xmlBlockIDField);
			this.xmlBlockTag = Const.NVL(XMLHandler.getTagValue(stepnode, "xmlBlockTag"), this.xmlBlockTag);

			this.includeXmlElementLevelField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeXmlElementLevelField"));
			this.xmlElementLevelField = Const.NVL(XMLHandler.getTagValue(stepnode, "xmlElementLevelField"), this.xmlElementLevelField);

			this.includeXmlPathField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeXmlPathField"));
			this.xmlPathField = Const.NVL(XMLHandler.getTagValue(stepnode, "xmlPathField"), this.xmlPathField);

			this.includeXmlParentPathField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeXmlParentPathField"));
			this.xmlParentPathField = Const.NVL(XMLHandler.getTagValue(stepnode, "xmlParentPathField"), this.xmlParentPathField);

			this.includeXmlDataNameField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeXmlDataNameField"));
			this.xmlDataNameField = Const.NVL(XMLHandler.getTagValue(stepnode, "xmlDataNameField"), this.xmlDataNameField);

			this.includeXmlDataValueField = "Y".equalsIgnoreCase(XMLHandler.getTagValue(stepnode, "includeXmlDataValueField"));
			this.xmlDataValueField = Const.NVL(XMLHandler.getTagValue(stepnode, "xmlDataValueField"), this.xmlDataValueField);

		}
		catch (Exception e) {
			throw new KettleXMLException("Unable to load step info from XML", e);
		}
	}

	@Override
	public Object clone()
	{
		XMLInputStreamMeta retval = (XMLInputStreamMeta) super.clone();
		// TODO check

		return retval;
	}

	@Override
	public String getXML()
	{
		StringBuffer retval = new StringBuffer();
		retval.append("    " + XMLHandler.addTagValue("filename", this.filename));
		retval.append("    " + XMLHandler.addTagValue("addResultFile", this.addResultFile));

		retval.append("    " + XMLHandler.addTagValue("nrRowsToSkip", this.nrRowsToSkip));
		retval.append("    " + XMLHandler.addTagValue("rowLimit", this.rowLimit));
		retval.append("    " + XMLHandler.addTagValue("defaultStringLen", this.defaultStringLen));
		retval.append("    " + XMLHandler.addTagValue("encoding", this.encoding));
		retval.append("    " + XMLHandler.addTagValue("enableNamespaces", this.enableNamespaces));
		retval.append("    " + XMLHandler.addTagValue("enableTrim", this.enableTrim));

		// The fields in the output stream
		retval.append("    " + XMLHandler.addTagValue("includeFilenameField", this.includeFilenameField));
		retval.append("    " + XMLHandler.addTagValue("filenameField", this.filenameField));

		retval.append("    " + XMLHandler.addTagValue("includeRowNumberField", this.includeRowNumberField));
		retval.append("    " + XMLHandler.addTagValue("rowNumberField", this.rowNumberField));

		retval.append("    " + XMLHandler.addTagValue("includeDataTypeNumericField", this.includeXmlDataTypeNumericField));
		retval.append("    " + XMLHandler.addTagValue("dataTypeNumericField", this.xmlDataTypeNumericField));

		retval.append("    " + XMLHandler.addTagValue("includeDataTypeDescriptionField", this.includeXmlDataTypeDescriptionField));
		retval.append("    " + XMLHandler.addTagValue("dataTypeDescriptionField", this.xmlDataTypeDescriptionField));

		retval.append("    " + XMLHandler.addTagValue("includeXmlLocationLineField", this.includeXmlLocationLineField));
		retval.append("    " + XMLHandler.addTagValue("xmlLocationLineField", this.xmlLocationLineField));

		retval.append("    " + XMLHandler.addTagValue("includeXmlLocationColumnField", this.includeXmlLocationColumnField));
		retval.append("    " + XMLHandler.addTagValue("xmlLocationColumnField", this.xmlLocationColumnField));

		retval.append("    " + XMLHandler.addTagValue("includeXmlElementIDField", this.includeXmlElementIDField));
		retval.append("    " + XMLHandler.addTagValue("xmlElementIDField", this.xmlElementIDField));

		retval.append("    " + XMLHandler.addTagValue("includeXmlParentElementIDField", this.includeXmlParentElementIDField));
		retval.append("    " + XMLHandler.addTagValue("xmlParentElementIDField", this.xmlParentElementIDField));

		retval.append("    " + XMLHandler.addTagValue("includeXmlBlockIDField", this.includeXmlBlockIDField));
		retval.append("    " + XMLHandler.addTagValue("xmlBlockIDField", this.xmlBlockIDField));
		retval.append("    " + XMLHandler.addTagValue("xmlBlockTag", this.xmlBlockTag));

		retval.append("    " + XMLHandler.addTagValue("includeXmlElementLevelField", this.includeXmlElementLevelField));
		retval.append("    " + XMLHandler.addTagValue("xmlElementLevelField", this.xmlElementLevelField));

		retval.append("    " + XMLHandler.addTagValue("includeXmlPathField", this.includeXmlPathField));
		retval.append("    " + XMLHandler.addTagValue("xmlPathField", this.xmlPathField));

		retval.append("    " + XMLHandler.addTagValue("includeXmlParentPathField", this.includeXmlParentPathField));
		retval.append("    " + XMLHandler.addTagValue("xmlParentPathField", this.xmlParentPathField));

		retval.append("    " + XMLHandler.addTagValue("includeXmlDataNameField", this.includeXmlDataNameField));
		retval.append("    " + XMLHandler.addTagValue("xmlDataNameField", this.xmlDataNameField));

		retval.append("    " + XMLHandler.addTagValue("includeXmlDataValueField", this.includeXmlDataValueField));
		retval.append("    " + XMLHandler.addTagValue("xmlDataValueField", this.xmlDataValueField));

		return retval.toString();
	}

	@Override
	public void setDefault()
	{
		this.filename = "";
		this.addResultFile = false;

		this.nrRowsToSkip = "0";
		this.rowLimit = "0";
		this.defaultStringLen = DEFAULT_STRING_LEN;
		this.encoding = DEFAULT_ENCODING;
		this.enableNamespaces = false;
		this.enableTrim = true;

		// The fields in the output stream
		this.includeFilenameField = false;
		this.filenameField = "xml_filename";

		this.includeRowNumberField = false;
		this.rowNumberField = "xml_row_number";

		this.includeXmlDataTypeNumericField = false;
		this.xmlDataTypeNumericField = "xml_data_type_numeric";

		this.includeXmlDataTypeDescriptionField = true;
		this.xmlDataTypeDescriptionField = "xml_data_type_description";

		this.includeXmlLocationLineField = false;
		this.xmlLocationLineField = "xml_location_line";

		this.includeXmlLocationColumnField = false;
		this.xmlLocationColumnField = "xml_location_column";

		this.includeXmlElementIDField = true;
		this.xmlElementIDField = "xml_element_id";

		this.includeXmlParentElementIDField = true;
		this.xmlParentElementIDField = "xml_parent_element_id";

		this.includeXmlBlockIDField = true;
		this.xmlBlockIDField = "xml_block_id";
		this.xmlBlockIDField = "xml_block_tag";

		this.includeXmlElementLevelField = true;
		this.xmlElementLevelField = "xml_element_level";

		this.includeXmlPathField = true;
		this.xmlPathField = "xml_path";

		this.includeXmlParentPathField = true;
		this.xmlParentPathField = "xml_parent_path";

		this.includeXmlDataNameField = true;
		this.xmlDataNameField = "xml_data_name";

		this.includeXmlDataValueField = true;
		this.xmlDataValueField = "xml_data_value";

	}

	@Override
	public void readRep(Repository rep, ObjectId id_step, List<DatabaseMeta> databases, Map<String, Counter> counters) throws KettleException
	{
		try {
			this.filename = Const.NVL(rep.getStepAttributeString(id_step, "filename"), "");
			this.addResultFile = rep.getStepAttributeBoolean(id_step, "addResultFile");

			this.nrRowsToSkip = Const.NVL(rep.getStepAttributeString(id_step, "nrRowsToSkip"), "0");
			this.rowLimit = Const.NVL(rep.getStepAttributeString(id_step, "rowLimit"), "0");
			this.defaultStringLen = Const.NVL(rep.getStepAttributeString(id_step, "defaultStringLen"), DEFAULT_STRING_LEN);
			this.encoding = Const.NVL(rep.getStepAttributeString(id_step, "encoding"), DEFAULT_ENCODING);
			this.enableNamespaces = rep.getStepAttributeBoolean(id_step, "enableNamespaces");
			this.enableTrim = rep.getStepAttributeBoolean(id_step, "enableTrim");

			// The fields in the output stream
			// When they are undefined (checked with NVL) the original default value will be taken
			this.includeFilenameField = rep.getStepAttributeBoolean(id_step, "includeFilenameField");
			this.filenameField = Const.NVL(rep.getStepAttributeString(id_step, "filenameField"), this.filenameField);

			this.includeRowNumberField = rep.getStepAttributeBoolean(id_step, "includeRowNumberField");
			this.rowNumberField = Const.NVL(rep.getStepAttributeString(id_step, "rowNumberField"), this.rowNumberField);

			this.includeXmlDataTypeNumericField = rep.getStepAttributeBoolean(id_step, "includeDataTypeNumericField");
			this.xmlDataTypeNumericField = Const.NVL(rep.getStepAttributeString(id_step, "dataTypeNumericField"), this.xmlDataTypeNumericField);

			this.includeXmlDataTypeDescriptionField = rep.getStepAttributeBoolean(id_step, "includeDataTypeDescriptionField");
			this.xmlDataTypeDescriptionField = Const.NVL(rep.getStepAttributeString(id_step, "dataTypeDescriptionField"), this.xmlDataTypeDescriptionField);

			this.includeXmlLocationLineField = rep.getStepAttributeBoolean(id_step, "includeXmlLocationLineField");
			this.xmlLocationLineField = Const.NVL(rep.getStepAttributeString(id_step, "xmlLocationLineField"), this.xmlLocationLineField);

			this.includeXmlLocationColumnField = rep.getStepAttributeBoolean(id_step, "includeXmlLocationColumnField");
			this.xmlLocationColumnField = Const.NVL(rep.getStepAttributeString(id_step, "xmlLocationColumnField"), this.xmlLocationColumnField);

			this.includeXmlElementIDField = rep.getStepAttributeBoolean(id_step, "includeXmlElementIDField");
			this.xmlElementIDField = Const.NVL(rep.getStepAttributeString(id_step, "xmlElementIDField"), this.xmlElementIDField);

			this.includeXmlParentElementIDField = rep.getStepAttributeBoolean(id_step, "includeXmlParentElementIDField");
			this.xmlParentElementIDField = Const.NVL(rep.getStepAttributeString(id_step, "xmlParentElementIDField"), this.xmlParentElementIDField);

			this.includeXmlBlockIDField = rep.getStepAttributeBoolean(id_step, "includeXmlBlockField");
			this.xmlBlockIDField = Const.NVL(rep.getStepAttributeString(id_step, "xmlBlockIDField"), this.xmlBlockIDField);
			this.xmlBlockTag = Const.NVL(rep.getStepAttributeString(id_step, "xmlBlockTag"), this.xmlBlockTag);

			this.includeXmlElementLevelField = rep.getStepAttributeBoolean(id_step, "includeXmlElementLevelField");
			this.xmlElementLevelField = Const.NVL(rep.getStepAttributeString(id_step, "xmlElementLevelField"), this.xmlElementLevelField);

			this.includeXmlPathField = rep.getStepAttributeBoolean(id_step, "includeXmlPathField");
			this.xmlPathField = Const.NVL(rep.getStepAttributeString(id_step, "xmlPathField"), this.xmlPathField);

			this.includeXmlParentPathField = rep.getStepAttributeBoolean(id_step, "includeXmlParentPathField");
			this.xmlParentPathField = Const.NVL(rep.getStepAttributeString(id_step, "xmlParentPathField"), this.xmlParentPathField);

			this.includeXmlDataNameField = rep.getStepAttributeBoolean(id_step, "includeXmlDataNameField");
			this.xmlDataNameField = Const.NVL(rep.getStepAttributeString(id_step, "xmlDataNameField"), this.xmlDataNameField);

			this.includeXmlDataValueField = rep.getStepAttributeBoolean(id_step, "includeXmlDataValueField");
			this.xmlDataValueField = Const.NVL(rep.getStepAttributeString(id_step, "xmlDataValueField"), this.xmlDataValueField);

		}
		catch (Exception e) {
			throw new KettleException("Unexpected error reading step information from the repository", e);
		}
	}

	@Override
	public void saveRep(Repository rep, ObjectId id_transformation, ObjectId id_step) throws KettleException
	{
		try {
			rep.saveStepAttribute(id_transformation, id_step, "filename", this.filename);
			rep.saveStepAttribute(id_transformation, id_step, "addResultFile", this.addResultFile);

			rep.saveStepAttribute(id_transformation, id_step, "nrRowsToSkip", this.nrRowsToSkip);
			rep.saveStepAttribute(id_transformation, id_step, "rowLimit", this.rowLimit);
			rep.saveStepAttribute(id_transformation, id_step, "defaultStringLen", this.defaultStringLen);
			rep.saveStepAttribute(id_transformation, id_step, "encoding", this.encoding);
			rep.saveStepAttribute(id_transformation, id_step, "enableNamespaces", this.enableNamespaces);
			rep.saveStepAttribute(id_transformation, id_step, "enableTrim", this.enableTrim);

			// The fields in the output stream
			rep.saveStepAttribute(id_transformation, id_step, "includeFilenameField", this.includeFilenameField);
			rep.saveStepAttribute(id_transformation, id_step, "filenameField", this.filenameField);

			rep.saveStepAttribute(id_transformation, id_step, "includeRowNumberField", this.includeRowNumberField);
			rep.saveStepAttribute(id_transformation, id_step, "rowNumberField", this.rowNumberField);

			rep.saveStepAttribute(id_transformation, id_step, "includeDataTypeNumericField", this.includeXmlDataTypeNumericField);
			rep.saveStepAttribute(id_transformation, id_step, "dataTypeNumericField", this.xmlDataTypeNumericField);

			rep.saveStepAttribute(id_transformation, id_step, "includeDataTypeDescriptionField", this.includeXmlDataTypeDescriptionField);
			rep.saveStepAttribute(id_transformation, id_step, "dataTypeDescriptionField", this.xmlDataTypeDescriptionField);

			rep.saveStepAttribute(id_transformation, id_step, "includeXmlLocationLineField", this.includeXmlLocationLineField);
			rep.saveStepAttribute(id_transformation, id_step, "xmlLocationLineField", this.xmlLocationLineField);

			rep.saveStepAttribute(id_transformation, id_step, "includeXmlLocationColumnField", this.includeXmlLocationColumnField);
			rep.saveStepAttribute(id_transformation, id_step, "xmlLocationColumnField", this.xmlLocationColumnField);

			rep.saveStepAttribute(id_transformation, id_step, "includeXmlElementIDField", this.includeXmlElementIDField);
			rep.saveStepAttribute(id_transformation, id_step, "xmlElementIDField", this.xmlElementIDField);

			rep.saveStepAttribute(id_transformation, id_step, "includeXmlParentElementIDField", this.includeXmlParentElementIDField);
			rep.saveStepAttribute(id_transformation, id_step, "xmlParentElementIDField", this.xmlParentElementIDField);

			rep.saveStepAttribute(id_transformation, id_step, "includeXmlBlockIDField", this.includeXmlBlockIDField);
			rep.saveStepAttribute(id_transformation, id_step, "xmlBlockIDField", this.xmlBlockIDField);
			rep.saveStepAttribute(id_transformation, id_step, "xmlBlockTag", this.xmlBlockTag);

			rep.saveStepAttribute(id_transformation, id_step, "includeXmlElementLevelField", this.includeXmlElementLevelField);
			rep.saveStepAttribute(id_transformation, id_step, "xmlElementLevelField", this.xmlElementLevelField);

			rep.saveStepAttribute(id_transformation, id_step, "includeXmlPathField", this.includeXmlPathField);
			rep.saveStepAttribute(id_transformation, id_step, "xmlPathField", this.xmlPathField);

			rep.saveStepAttribute(id_transformation, id_step, "includeXmlParentPathField", this.includeXmlParentPathField);
			rep.saveStepAttribute(id_transformation, id_step, "xmlParentPathField", this.xmlParentPathField);

			rep.saveStepAttribute(id_transformation, id_step, "includeXmlDataNameField", this.includeXmlDataNameField);
			rep.saveStepAttribute(id_transformation, id_step, "xmlDataNameField", this.xmlDataNameField);

			rep.saveStepAttribute(id_transformation, id_step, "includeXmlDataValueField", this.includeXmlDataValueField);
			rep.saveStepAttribute(id_transformation, id_step, "xmlDataValueField", this.xmlDataValueField);

		}
		catch (Exception e) {
			throw new KettleException("Unable to save step information to the repository for id_step=" + id_step, e);
		}
	}

	@Override
	public void check(List<CheckResultInterface> remarks, TransMeta transMeta, StepMeta stepinfo, RowMetaInterface prev, String input[], String output[], RowMetaInterface info)
	{
		// TODO externalize messages
		CheckResult cr;
		if (Const.isEmpty(this.filename)) {
			cr = new CheckResult(CheckResultInterface.TYPE_RESULT_ERROR, "Filename is not given", stepinfo);
		}
		else {
			cr = new CheckResult(CheckResultInterface.TYPE_RESULT_OK, "Filename is given", stepinfo);
		}
		remarks.add(cr);

		if (this.includeXmlDataTypeNumericField || this.includeXmlDataTypeDescriptionField) {
			cr = new CheckResult(CheckResultInterface.TYPE_RESULT_COMMENT, "At least one Data Type field (numeric or description) is in the data stream", stepinfo);
		}
		else {
			cr = new CheckResult(CheckResultInterface.TYPE_RESULT_WARNING, "Data Type field (numeric or description) is missing in the data stream", stepinfo);
		}
		remarks.add(cr);

		if (this.includeXmlDataValueField && this.includeXmlDataNameField) {
			cr = new CheckResult(CheckResultInterface.TYPE_RESULT_COMMENT, "Data Name and Data Value fields are in the data stream", stepinfo);
		}
		else {
			cr = new CheckResult(CheckResultInterface.TYPE_RESULT_WARNING, "Both Data Name and Data Value fields should be in the data stream", stepinfo);
		}
		remarks.add(cr);
	}

	@Override
	public StepInterface getStep(StepMeta stepMeta, StepDataInterface stepDataInterface, int cnr, TransMeta transMeta, Trans trans)
	{
		return new XMLInputStream(stepMeta, stepDataInterface, cnr, transMeta, trans);
	}

	@Override
	public StepDataInterface getStepData()
	{
		return new XMLInputStreamData();
	}

	public String getFilename()
	{
		return this.filename;
	}

	public void setFilename(String filename)
	{
		this.filename = filename;
	}

	public boolean isAddResultFile()
	{
		return this.addResultFile;
	}

	public void setAddResultFile(boolean addResultFile)
	{
		this.addResultFile = addResultFile;
	}

	public String getNrRowsToSkip()
	{
		return this.nrRowsToSkip;
	}

	public void setNrRowsToSkip(String nrRowsToSkip)
	{
		this.nrRowsToSkip = nrRowsToSkip;
	}

	public String getRowLimit()
	{
		return this.rowLimit;
	}

	public void setRowLimit(String rowLimit)
	{
		this.rowLimit = rowLimit;
	}

	public String getDefaultStringLen()
	{
		return this.defaultStringLen;
	}

	public void setDefaultStringLen(String defaultStringLen)
	{
		this.defaultStringLen = defaultStringLen;
	}

	public String getEncoding()
	{
		return this.encoding;
	}

	public void setEncoding(String encoding)
	{
		this.encoding = encoding;
	}

	public boolean isEnableNamespaces()
	{
		return this.enableNamespaces;
	}

	public void setEnableNamespaces(boolean enableNamespaces)
	{
		this.enableNamespaces = enableNamespaces;
	}

	public boolean isEnableTrim()
	{
		return this.enableTrim;
	}

	public void setEnableTrim(boolean enableTrim)
	{
		this.enableTrim = enableTrim;
	}

	public boolean isIncludeFilenameField()
	{
		return this.includeFilenameField;
	}

	public void setIncludeFilenameField(boolean includeFilenameField)
	{
		this.includeFilenameField = includeFilenameField;
	}

	public String getFilenameField()
	{
		return this.filenameField;
	}

	public void setFilenameField(String filenameField)
	{
		this.filenameField = filenameField;
	}

	public boolean isIncludeRowNumberField()
	{
		return this.includeRowNumberField;
	}

	public void setIncludeRowNumberField(boolean includeRowNumberField)
	{
		this.includeRowNumberField = includeRowNumberField;
	}

	public String getRowNumberField()
	{
		return this.rowNumberField;
	}

	public void setRowNumberField(String rowNumberField)
	{
		this.rowNumberField = rowNumberField;
	}

	public boolean isIncludeXmlDataTypeNumericField()
	{
		return this.includeXmlDataTypeNumericField;
	}

	public void setIncludeXmlDataTypeNumericField(boolean includeXmlDataTypeNumericField)
	{
		this.includeXmlDataTypeNumericField = includeXmlDataTypeNumericField;
	}

	public String getXmlDataTypeNumericField()
	{
		return this.xmlDataTypeNumericField;
	}

	public void setXmlDataTypeNumericField(String xmlDataTypeNumericField)
	{
		this.xmlDataTypeNumericField = xmlDataTypeNumericField;
	}

	public boolean isIncludeXmlDataTypeDescriptionField()
	{
		return this.includeXmlDataTypeDescriptionField;
	}

	public void setIncludeXmlDataTypeDescriptionField(boolean includeXmlDataTypeDescriptionField)
	{
		this.includeXmlDataTypeDescriptionField = includeXmlDataTypeDescriptionField;
	}

	public String getXmlDataTypeDescriptionField()
	{
		return this.xmlDataTypeDescriptionField;
	}

	public void setXmlDataTypeDescriptionField(String xmlDataTypeDescriptionField)
	{
		this.xmlDataTypeDescriptionField = xmlDataTypeDescriptionField;
	}

	public boolean isIncludeXmlLocationLineField()
	{
		return this.includeXmlLocationLineField;
	}

	public void setIncludeXmlLocationLineField(boolean includeXmlLocationLineField)
	{
		this.includeXmlLocationLineField = includeXmlLocationLineField;
	}

	public String getXmlLocationLineField()
	{
		return this.xmlLocationLineField;
	}

	public void setXmlLocationLineField(String xmlLocationLineField)
	{
		this.xmlLocationLineField = xmlLocationLineField;
	}

	public boolean isIncludeXmlLocationColumnField()
	{
		return this.includeXmlLocationColumnField;
	}

	public void setIncludeXmlLocationColumnField(boolean includeXmlLocationColumnField)
	{
		this.includeXmlLocationColumnField = includeXmlLocationColumnField;
	}

	public String getXmlLocationColumnField()
	{
		return this.xmlLocationColumnField;
	}

	public void setXmlLocationColumnField(String xmlLocationColumnField)
	{
		this.xmlLocationColumnField = xmlLocationColumnField;
	}

	public boolean isIncludeXmlElementIDField()
	{
		return this.includeXmlElementIDField;
	}

	public void setIncludeXmlElementIDField(boolean includeXmlElementIDField)
	{
		this.includeXmlElementIDField = includeXmlElementIDField;
	}

	public String getXmlElementIDField()
	{
		return this.xmlElementIDField;
	}

	public void setXmlElementIDField(String xmlElementIDField)
	{
		this.xmlElementIDField = xmlElementIDField;
	}

	public boolean isIncludeXmlParentElementIDField()
	{
		return this.includeXmlParentElementIDField;
	}

	public void setIncludeXmlParentElementIDField(boolean includeXmlParentElementIDField)
	{
		this.includeXmlParentElementIDField = includeXmlParentElementIDField;
	}

	public String getXmlParentElementIDField()
	{
		return this.xmlParentElementIDField;
	}

	public void setXmlParentElementIDField(String xmlParentElementIDField)
	{
		this.xmlParentElementIDField = xmlParentElementIDField;
	}

	public boolean isIncludeXmlBlockIDField()
	{
		return this.includeXmlBlockIDField;
	}

	public void setIncludeXmlBlockIDField(boolean includeXmlBlockIDField)
	{
		this.includeXmlBlockIDField = includeXmlBlockIDField;
	}

	public String getXmlBlockIDField()
	{
		return this.xmlBlockIDField;
	}

	public void setXmlBlockIDField(String xmlBlockIDField)
	{
		this.xmlBlockIDField = xmlBlockIDField;
	}

	public boolean isIncludeXmlElementLevelField()
	{
		return this.includeXmlElementLevelField;
	}

	public void setIncludeXmlElementLevelField(boolean includeXmlElementLevelField)
	{
		this.includeXmlElementLevelField = includeXmlElementLevelField;
	}

	public String getXmlElementLevelField()
	{
		return this.xmlElementLevelField;
	}

	public void setXmlElementLevelField(String xmlElementLevelField)
	{
		this.xmlElementLevelField = xmlElementLevelField;
	}

	public boolean isIncludeXmlPathField()
	{
		return this.includeXmlPathField;
	}

	public void setIncludeXmlPathField(boolean includeXmlPathField)
	{
		this.includeXmlPathField = includeXmlPathField;
	}

	public String getXmlPathField()
	{
		return this.xmlPathField;
	}

	public void setXmlPathField(String xmlPathField)
	{
		this.xmlPathField = xmlPathField;
	}

	public boolean isIncludeXmlParentPathField()
	{
		return this.includeXmlParentPathField;
	}

	public void setIncludeXmlParentPathField(boolean includeXmlParentPathField)
	{
		this.includeXmlParentPathField = includeXmlParentPathField;
	}

	public String getXmlParentPathField()
	{
		return this.xmlParentPathField;
	}

	public void setXmlParentPathField(String xmlParentPathField)
	{
		this.xmlParentPathField = xmlParentPathField;
	}

	public boolean isIncludeXmlDataNameField()
	{
		return this.includeXmlDataNameField;
	}

	public void setIncludeXmlDataNameField(boolean includeXmlDataNameField)
	{
		this.includeXmlDataNameField = includeXmlDataNameField;
	}

	public String getXmlDataNameField()
	{
		return this.xmlDataNameField;
	}

	public void setXmlDataNameField(String xmlDataNameField)
	{
		this.xmlDataNameField = xmlDataNameField;
	}

	public boolean isIncludeXmlDataValueField()
	{
		return this.includeXmlDataValueField;
	}

	public void setIncludeXmlDataValueField(boolean includeXmlDataValueField)
	{
		this.includeXmlDataValueField = includeXmlDataValueField;
	}

	public String getXmlDataValueField()
	{
		return this.xmlDataValueField;
	}

	public void setXmlDataValueField(String xmlDataValueField)
	{
		this.xmlDataValueField = xmlDataValueField;
	}

	public String getXmlBlockTag()
	{
		return this.xmlBlockTag;
	}

	public void setXmlBlockTag(String xmlBlockTag)
	{
		this.xmlBlockTag = xmlBlockTag;
	}

}
