/*******************************************************************************
 *
 * Pentaho Data Integration
 *
 * Copyright (C) 2002-2012 by Pentaho : http://www.pentaho.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/

package org.pentaho.di.trans.steps.xmlinputstream;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Namespace;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.io.FileUtils;
import org.apache.commons.vfs.FileSystemException;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.ResultFile;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleStepException;
import org.pentaho.di.core.exception.KettleValueException;
import org.pentaho.di.core.row.RowDataUtil;
import org.pentaho.di.core.row.RowMeta;
import org.pentaho.di.core.vfs.KettleVFS;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.step.BaseStep;
import org.pentaho.di.trans.step.StepDataInterface;
import org.pentaho.di.trans.step.StepInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.StepMetaInterface;

/**
 * Use a StAX parser to read XML in a flexible and fast way.
 * 
 * @author Jens Bleuel
 * @since 2011-01-13
 */
//TODO black box testing
public class XMLInputStream extends BaseStep implements StepInterface
{
	private static Class<?> PKG = XMLInputStream.class; // for i18n purposes, needed by Translator2!!   $NON-NLS-1$

	private static int PARENT_ID_ALLOCATE_SIZE = 1000; //max. number of nested elements, we may let the user configure this

	private XMLInputStreamMeta meta;

	private XMLInputStreamData data;

	private Long parentId = new Long(0);

	static final String[] eventDescription = { "UNKNOWN", "START_ELEMENT", "END_ELEMENT", "PROCESSING_INSTRUCTION", "CHARACTERS", "COMMENT", "SPACE", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
		"START_DOCUMENT", "END_DOCUMENT", "ENTITY_REFERENCE", "ATTRIBUTE", "DTD", "CDATA", "NAMESPACE", //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$ //$NON-NLS-6$ //$NON-NLS-7$
		"NOTATION_DECLARATION", "ENTITY_DECLARATION" }; //$NON-NLS-1$ //$NON-NLS-2$

	public XMLInputStream(StepMeta stepMeta, StepDataInterface stepDataInterface, int copyNr, TransMeta transMeta, Trans trans)
	{
		super(stepMeta, stepDataInterface, copyNr, transMeta, trans);
	}

	@Override
	public boolean processRow(StepMetaInterface smi, StepDataInterface sdi) throws KettleException
	{
		if (this.first) {
			this.first = false;

			// replace variables
			this.data.filename = this.environmentSubstitute(this.meta.getFilename());
			this.data.nrRowsToSkip = Const.toLong(this.environmentSubstitute(this.meta.getNrRowsToSkip()), 0);
			this.data.rowLimit = Const.toLong(this.environmentSubstitute(this.meta.getRowLimit()), 0);
			this.data.encoding = this.environmentSubstitute(this.meta.getEncoding());

			this.data.outputRowMeta = new RowMeta();
			this.meta.getFields(this.data.outputRowMeta, this.getStepname(), null, null, this);
			// get and save field positions
			this.data.pos_xml_filename = this.data.outputRowMeta.indexOfValue(this.meta.getFilenameField());
			this.data.pos_xml_row_number = this.data.outputRowMeta.indexOfValue(this.meta.getRowNumberField());
			this.data.pos_xml_data_type_numeric = this.data.outputRowMeta.indexOfValue(this.meta.getXmlDataTypeNumericField());
			this.data.pos_xml_data_type_description = this.data.outputRowMeta.indexOfValue(this.meta.getXmlDataTypeDescriptionField());
			this.data.pos_xml_location_line = this.data.outputRowMeta.indexOfValue(this.meta.getXmlLocationLineField());
			this.data.pos_xml_location_column = this.data.outputRowMeta.indexOfValue(this.meta.getXmlLocationColumnField());
			this.data.pos_xml_element_id = this.data.outputRowMeta.indexOfValue(this.meta.getXmlElementIDField());
			this.data.pos_xml_parent_element_id = this.data.outputRowMeta.indexOfValue(this.meta.getXmlParentElementIDField());
			this.data.pos_xml_block_id = this.data.outputRowMeta.indexOfValue(this.meta.getXmlBlockIDField());
			this.data.pos_xml_element_level = this.data.outputRowMeta.indexOfValue(this.meta.getXmlElementLevelField());
			this.data.pos_xml_path = this.data.outputRowMeta.indexOfValue(this.meta.getXmlPathField());
			this.data.pos_xml_parent_path = this.data.outputRowMeta.indexOfValue(this.meta.getXmlParentPathField());
			this.data.pos_xml_data_name = this.data.outputRowMeta.indexOfValue(this.meta.getXmlDataNameField());
			this.data.pos_xml_data_value = this.data.outputRowMeta.indexOfValue(this.meta.getXmlDataValueField());

			this.data.fileObject = KettleVFS.getFileObject(this.data.filename, this.getTransMeta());

			try {
				this.data.fileInputStream = new FileInputStream(FileUtils.toFile(this.data.fileObject.getURL()));
			}
			catch (FileNotFoundException e) { // by FileInputStream
				throw new KettleException(e);
			}
			catch (FileSystemException e) { // by fileObject.getURL()
				throw new KettleException(e);
			}
			try {
				this.data.xmlEventReader = this.data.staxInstance.createXMLEventReader(this.data.fileInputStream, this.data.encoding);
			}
			catch (XMLStreamException e) {
				throw new KettleException(e);
			}

			if (this.meta.isAddResultFile()) {
				// Add this to the result file names...
				ResultFile resultFile = new ResultFile(ResultFile.FILE_TYPE_GENERAL, this.data.fileObject, this.getTransMeta().getName(), this.getStepname());
				resultFile.setComment("File was read by an XML Input Stream step"); //TODO externalize
				this.addResultFile(resultFile);
			}

			this.resetElementCounters();
		}

		Object[] outputRowData = this.getRowFromXML();
		if (outputRowData == null) {
			this.setOutputDone(); // signal end to receiver(s)
			return false; // This is the end of this step.
		}

		this.putRowOut(outputRowData);

		// limit has been reached: stop now. (not exact science since some attributes could be mixed within the last row)
		if (this.data.rowLimit > 0 && this.data.rowNumber >= this.data.rowLimit) {
			this.setOutputDone();
			return false;
		}

		return true;
	}

	// sends the normal row and attributes
	private void putRowOut(Object r[]) throws KettleStepException, KettleValueException
	{

		this.data.rowNumber++;
		if (this.data.pos_xml_filename != -1) {
			r[this.data.pos_xml_filename] = new String(this.data.filename);
		}
		if (this.data.pos_xml_row_number != -1) {
			r[this.data.pos_xml_row_number] = new Long(this.data.rowNumber);
		}
		if (this.data.pos_xml_element_id != -1) {
			r[this.data.pos_xml_element_id] = this.data.elementLevelID[this.data.elementLevel];
		}
		if (this.data.pos_xml_element_level != -1) {
			r[this.data.pos_xml_element_level] = new Long(this.data.elementLevel);
		}
		if (this.data.pos_xml_parent_element_id != -1) {
			r[this.data.pos_xml_parent_element_id] = this.data.elementParentID[this.data.elementLevel];
		}
		if (this.data.pos_xml_block_id != -1) {
			r[this.data.pos_xml_block_id] = this.parentId;
		}
		if (this.data.pos_xml_path != -1) {
			r[this.data.pos_xml_path] = this.data.elementPath[this.data.elementLevel];
		}
		if (this.data.pos_xml_parent_path != -1 && this.data.elementLevel > 0) {
			r[this.data.pos_xml_parent_path] = this.data.elementPath[this.data.elementLevel - 1];
		}

		// We could think of adding an option to filter Start_end Document / Elements, RegEx?
		// We could think of adding columns identifying Element-Blocks

		// Skip rows? (not exact science since some attributes could be mixed within the last row)
		if (this.data.nrRowsToSkip == 0 || this.data.rowNumber > this.data.nrRowsToSkip) {
			if (this.log.isRowLevel()) {
				this.logRowlevel("Read row: " + this.data.outputRowMeta.getString(r)); //$NON-NLS-1$
			}
			this.putRow(this.data.outputRowMeta, r);
		}
	}

	private Object[] getRowFromXML() throws KettleException
	{

		Object[] outputRowData = null;
		// loop until significant data is there and more data is there
		while (this.data.xmlEventReader.hasNext() && outputRowData == null && !this.isStopped()) {
			outputRowData = this.processEvent();
			// log all events (but no attributes sent by the EventReader)
			this.incrementLinesInput();
			if (this.checkFeedback(this.getLinesInput()) && this.isBasic()) {
				this.logBasic(BaseMessages.getString(PKG, "XMLInputStream.Log.LineNumber", Long.toString(this.getLinesInput()))); //$NON-NLS-1$
			}
		}

		return outputRowData;
	}

	private Object[] processEvent() throws KettleException
	{

		Object[] outputRowData = RowDataUtil.allocateRowData(this.data.outputRowMeta.size());
		XMLEvent e = null;
		try {
			e = this.data.xmlEventReader.nextEvent();
		}
		catch (XMLStreamException ex) {
			throw new KettleException(ex);
		}

		int eventType = e.getEventType();
		if (this.data.pos_xml_data_type_numeric != -1) {
			outputRowData[this.data.pos_xml_data_type_numeric] = new Long(eventType);
		}
		if (this.data.pos_xml_data_type_description != -1) {
			if (eventType == 0 || eventType > eventDescription.length) {
				//unknown eventType
				outputRowData[this.data.pos_xml_data_type_description] = eventDescription[0] + "(" + eventType + ")";
			}
			else {
				outputRowData[this.data.pos_xml_data_type_description] = eventDescription[eventType];
			}
		}
		if (this.data.pos_xml_location_line != -1) {
			outputRowData[this.data.pos_xml_location_line] = new Long(e.getLocation().getLineNumber());
		}
		if (this.data.pos_xml_location_column != -1) {
			outputRowData[this.data.pos_xml_location_column] = new Long(e.getLocation().getColumnNumber());
		}

		switch (eventType) {

			case XMLStreamConstants.START_ELEMENT:
				this.data.elementLevel++;
				if (this.data.elementLevel > PARENT_ID_ALLOCATE_SIZE - 1) {
					throw new KettleException("Too many nested XML elements, more than " + PARENT_ID_ALLOCATE_SIZE);
				}
				if (this.data.elementParentID[this.data.elementLevel] == null) {
					this.data.elementParentID[this.data.elementLevel] = this.data.elementID;
				}

				if (e.asStartElement().getName().getLocalPart().equalsIgnoreCase(this.meta.getXmlBlockTag())) {
					this.parentId++;
				}

				this.data.elementID++;
				this.data.elementLevelID[this.data.elementLevel] = this.data.elementID;

				if (this.meta.isEnableNamespaces()) {
					String prefix = e.asStartElement().getName().getPrefix();
					if (Const.isEmpty(prefix)) {
						outputRowData[this.data.pos_xml_data_name] = e.asStartElement().getName().getLocalPart();
					}
					else { // add namespace prefix:
						outputRowData[this.data.pos_xml_data_name] = prefix + ":" + e.asStartElement().getName().getLocalPart();
					}
				}
				else {
					outputRowData[this.data.pos_xml_data_name] = e.asStartElement().getName().getLocalPart();
				}

				//store the name
				this.data.elementName[this.data.elementLevel] = new String((String) outputRowData[this.data.pos_xml_data_name]);
				//store simple path
				this.data.elementPath[this.data.elementLevel] = this.data.elementPath[this.data.elementLevel - 1] + "/" + outputRowData[this.data.pos_xml_data_name];

				// write Namespaces out
				if (this.meta.isEnableNamespaces()) {
					outputRowData = this.parseNamespaces(outputRowData, e);
				}

				// write Attributes out
				outputRowData = this.parseAttributes(outputRowData, e);

				break;

			case XMLStreamConstants.END_ELEMENT:
				outputRowData[this.data.pos_xml_data_name] = e.asEndElement().getName().getLocalPart();
				this.putRowOut(outputRowData);
				this.data.elementParentID[this.data.elementLevel + 1] = null;
				this.data.elementLevel--;
				outputRowData = null; // continue
				break;

			case XMLStreamConstants.SPACE:
				outputRowData = null; // ignore & continue
				break;

			case XMLStreamConstants.CHARACTERS:
				outputRowData[this.data.pos_xml_data_name] = this.data.elementName[this.data.elementLevel];
				outputRowData[this.data.pos_xml_data_value] = e.asCharacters().getData();
				//optional trim is also eliminating white spaces, tab, cr, lf
				if (this.meta.isEnableTrim()) {
					outputRowData[this.data.pos_xml_data_value] = Const.trim((String) outputRowData[this.data.pos_xml_data_value]);
				}
				if (Const.isEmpty((String) outputRowData[this.data.pos_xml_data_value])) {
					outputRowData = null; // ignore & continue
				}
				break;

			case XMLStreamConstants.PROCESSING_INSTRUCTION:
				outputRowData = null; // ignore & continue
				//TODO test if possible
				break;

			case XMLStreamConstants.CDATA:
				// normally this is automatically in CHARACTERS
				outputRowData[this.data.pos_xml_data_name] = this.data.elementName[this.data.elementLevel];
				outputRowData[this.data.pos_xml_data_value] = e.asCharacters().getData();
				//optional trim is also eliminating white spaces, tab, cr, lf
				if (this.meta.isEnableTrim()) {
					outputRowData[this.data.pos_xml_data_value] = Const.trim((String) outputRowData[this.data.pos_xml_data_value]);
				}
				if (Const.isEmpty((String) outputRowData[this.data.pos_xml_data_value])) {
					outputRowData = null; // ignore & continue
				}
				break;

			case XMLStreamConstants.COMMENT:
				outputRowData = null; // ignore & continue
				//TODO test if possible
				break;

			case XMLStreamConstants.ENTITY_REFERENCE:
				// should be resolved by default
				outputRowData = null; // ignore & continue
				break;

			case XMLStreamConstants.START_DOCUMENT:
				// just get this information out
				break;

			case XMLStreamConstants.END_DOCUMENT:
				// just get this information out
				break;

			default:
				this.logBasic("Event:" + eventType);
				outputRowData = null; // ignore & continue
		}

		return outputRowData;
	}

	// Namespaces: put an extra row out for each namespace
	@SuppressWarnings("unchecked")
	private Object[] parseNamespaces(Object[] outputRowData, XMLEvent e) throws KettleValueException, KettleStepException
	{
		Iterator<Namespace> iter = e.asStartElement().getNamespaces();
		if (iter.hasNext()) {
			Object[] outputRowDataNamespace = this.data.outputRowMeta.cloneRow(outputRowData);
			this.putRowOut(outputRowDataNamespace); // first put the element name info out 
			// change data_type to ATTRIBUTE
			if (this.data.pos_xml_data_type_numeric != -1) {
				outputRowData[this.data.pos_xml_data_type_numeric] = new Long(XMLStreamConstants.NAMESPACE);
			}
			if (this.data.pos_xml_data_type_description != -1) {
				outputRowData[this.data.pos_xml_data_type_description] = eventDescription[XMLStreamConstants.NAMESPACE];
			}
		}
		while (iter.hasNext()) {
			Object[] outputRowDataNamespace = this.data.outputRowMeta.cloneRow(outputRowData);
			Namespace n = iter.next();
			outputRowDataNamespace[this.data.pos_xml_data_name] = n.getPrefix();
			outputRowDataNamespace[this.data.pos_xml_data_value] = n.getNamespaceURI();
			if (iter.hasNext()) {
				// send out the Namespace row
				this.putRowOut(outputRowDataNamespace);
			}
			else {
				// last row: this will be sent out by the outer loop
				outputRowData = outputRowDataNamespace;
			}
		}

		return outputRowData;
	}

	// Attributes: put an extra row out for each attribute
	@SuppressWarnings("unchecked")
	private Object[] parseAttributes(Object[] outputRowData, XMLEvent e) throws KettleValueException, KettleStepException
	{
		Iterator<Attribute> iter = e.asStartElement().getAttributes();
		if (iter.hasNext()) {
			Object[] outputRowDataAttribute = this.data.outputRowMeta.cloneRow(outputRowData);
			this.putRowOut(outputRowDataAttribute); // first put the element name (or namespace) info out 
			// change data_type to ATTRIBUTE
			if (this.data.pos_xml_data_type_numeric != -1) {
				outputRowData[this.data.pos_xml_data_type_numeric] = new Long(XMLStreamConstants.ATTRIBUTE);
			}
			if (this.data.pos_xml_data_type_description != -1) {
				outputRowData[this.data.pos_xml_data_type_description] = eventDescription[XMLStreamConstants.ATTRIBUTE];
			}
		}
		while (iter.hasNext()) {
			Object[] outputRowDataAttribute = this.data.outputRowMeta.cloneRow(outputRowData);
			Attribute a = iter.next();
			outputRowDataAttribute[this.data.pos_xml_data_name] = a.getName().getLocalPart();
			outputRowDataAttribute[this.data.pos_xml_data_value] = a.getValue();
			if (iter.hasNext()) {
				// send out the Attribute row
				this.putRowOut(outputRowDataAttribute);
			}
			else {
				// last row: this will be sent out by the outer loop
				outputRowData = outputRowDataAttribute;
			}
		}

		return outputRowData;
	}

	private void resetElementCounters()
	{
		this.data.rowNumber = new Long(0);
		this.data.elementLevel = 0;
		this.data.elementID = new Long(0); // init value, could be parameterized later on
		this.data.elementLevelID = new Long[PARENT_ID_ALLOCATE_SIZE];
		this.data.elementLevelID[0] = this.data.elementID; //inital id for level 0
		this.data.elementParentID = new Long[PARENT_ID_ALLOCATE_SIZE];
		this.data.elementBlockID = new Long[PARENT_ID_ALLOCATE_SIZE];
		this.data.elementName = new String[PARENT_ID_ALLOCATE_SIZE];
		this.data.elementPath = new String[PARENT_ID_ALLOCATE_SIZE];
		this.data.elementPath[0] = ""; //initial empty
	}

	@Override
	public boolean init(StepMetaInterface smi, StepDataInterface sdi)
	{
		this.meta = (XMLInputStreamMeta) smi;
		this.data = (XMLInputStreamData) sdi;

		if (super.init(smi, sdi)) {
			this.data.staxInstance = XMLInputFactory.newInstance(); // could select the parser later on
			return true;
		}
		return false;
	}

	@Override
	public void dispose(StepMetaInterface smi, StepDataInterface sdi)
	{
		this.meta = (XMLInputStreamMeta) smi;
		this.data = (XMLInputStreamData) sdi;

		// free resources
		if (this.data.xmlEventReader != null) {
			try {
				this.data.xmlEventReader.close();
			}
			catch (XMLStreamException e) {
				// intentionally ignored on closing
			}
			this.data.xmlEventReader = null;
		}
		if (this.data.fileInputStream != null) {
			try {
				this.data.fileInputStream.close();
			}
			catch (IOException e) {
				// intentionally ignored on closing
			}
			this.data.fileInputStream = null;
		}
		if (this.data.fileObject != null) {
			try {
				this.data.fileObject.close();
			}
			catch (IOException e) {
				// intentionally ignored on closing
			}
			this.data.fileObject = null;
		}

		this.data.staxInstance = null;

		super.dispose(smi, sdi);
	}

}
