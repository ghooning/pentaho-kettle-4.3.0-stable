/*******************************************************************************
 *
 * Pentaho Data Integration
 *
 * Copyright (C) 2002-2012 by Pentaho : http://www.pentaho.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/

package org.pentaho.di.job.entries.sftpput;

import static org.pentaho.di.job.entry.validator.AndValidator.putValidators;
import static org.pentaho.di.job.entry.validator.JobEntryValidatorUtils.andValidator;
import static org.pentaho.di.job.entry.validator.JobEntryValidatorUtils.fileExistsValidator;
import static org.pentaho.di.job.entry.validator.JobEntryValidatorUtils.integerValidator;
import static org.pentaho.di.job.entry.validator.JobEntryValidatorUtils.notBlankValidator;
import static org.pentaho.di.job.entry.validator.JobEntryValidatorUtils.notNullValidator;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.vfs.FileObject;
import org.apache.commons.vfs.FileType;
import org.pentaho.di.cluster.SlaveServer;
import org.pentaho.di.core.CheckResultInterface;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.Result;
import org.pentaho.di.core.ResultFile;
import org.pentaho.di.core.RowMetaAndData;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.encryption.Encr;
import org.pentaho.di.core.exception.KettleDatabaseException;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.core.vfs.KettleVFS;
import org.pentaho.di.core.xml.XMLHandler;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.job.JobMeta;
import org.pentaho.di.job.entries.sftp.SFTPClient;
import org.pentaho.di.job.entry.JobEntryBase;
import org.pentaho.di.job.entry.JobEntryInterface;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.resource.ResourceEntry;
import org.pentaho.di.resource.ResourceEntry.ResourceType;
import org.pentaho.di.resource.ResourceReference;
import org.w3c.dom.Node;

/**
 * This defines an SFTP put job entry.
 *
 * @author Matt
 * @since 05-11-2003
 *
 */
public class JobEntrySFTPPUT extends JobEntryBase implements Cloneable, JobEntryInterface
{
	private static Class<?> PKG = JobEntrySFTPPUT.class; // for i18n purposes, needed by Translator2!!   $NON-NLS-1$

	private int afterFTPS;

	public static final String[] afterFTPSDesc = new String[] { BaseMessages.getString(PKG, "JobSFTPPUT.AfterSFTP.DoNothing.Label"), BaseMessages.getString(PKG, "JobSFTPPUT.AfterSFTP.Delete.Label"),
		BaseMessages.getString(PKG, "JobSFTPPUT.AfterSFTP.Move.Label"), };

	public static final String[] afterFTPSCode = new String[] { "nothing", "delete", "move" };

	public static final int AFTER_FTPSPUT_NOTHING = 0;

	public static final int AFTER_FTPSPUT_DELETE = 1;

	public static final int AFTER_FTPSPUT_MOVE = 2;

	private String serverName;

	private String serverPort;

	private String userName;

	private String password;

	private String sftpDirectory;

	private String localDirectory;

	private String wildcard;

	private boolean copyprevious;

	private boolean addFilenameResut;

	private boolean usekeyfilename;

	private String keyfilename;

	private String keyfilepass;

	private String compression;

	private boolean createRemoteFolder;

	// proxy
	private String proxyType;

	private String proxyHost;

	private String proxyPort;

	private String proxyUsername;

	private String proxyPassword;

	private String destinationfolder;

	private boolean createDestinationFolder;

	private boolean successWhenNoFile;

	public JobEntrySFTPPUT(String n)
	{
		super(n, "");
		this.serverName = null;
		this.serverPort = "22";
		this.copyprevious = false;
		this.addFilenameResut = false;
		this.usekeyfilename = false;
		this.keyfilename = null;
		this.keyfilepass = null;
		this.compression = "none";
		this.proxyType = null;
		this.proxyHost = null;
		this.proxyPort = null;
		this.proxyUsername = null;
		this.proxyPassword = null;
		this.createRemoteFolder = false;
		this.afterFTPS = AFTER_FTPSPUT_NOTHING;
		this.destinationfolder = null;
		this.createDestinationFolder = false;
		this.successWhenNoFile = false;
		this.setID(-1L);
	}

	public JobEntrySFTPPUT()
	{
		this("");
	}

	@Override
	public Object clone()
	{
		JobEntrySFTPPUT je = (JobEntrySFTPPUT) super.clone();
		return je;
	}

	@Override
	public String getXML()
	{
		StringBuffer retval = new StringBuffer(300);

		retval.append(super.getXML());

		retval.append("      ").append(XMLHandler.addTagValue("servername", this.serverName));
		retval.append("      ").append(XMLHandler.addTagValue("serverport", this.serverPort));
		retval.append("      ").append(XMLHandler.addTagValue("username", this.userName));
		retval.append("      ").append(XMLHandler.addTagValue("password", Encr.encryptPasswordIfNotUsingVariables(this.password)));
		retval.append("      ").append(XMLHandler.addTagValue("sftpdirectory", this.sftpDirectory));
		retval.append("      ").append(XMLHandler.addTagValue("localdirectory", this.localDirectory));
		retval.append("      ").append(XMLHandler.addTagValue("wildcard", this.wildcard));
		retval.append("      ").append(XMLHandler.addTagValue("copyprevious", this.copyprevious));
		retval.append("      ").append(XMLHandler.addTagValue("addFilenameResut", this.addFilenameResut));
		retval.append("      ").append(XMLHandler.addTagValue("usekeyfilename", this.usekeyfilename));
		retval.append("      ").append(XMLHandler.addTagValue("keyfilename", this.keyfilename));
		retval.append("      ").append(XMLHandler.addTagValue("keyfilepass", Encr.encryptPasswordIfNotUsingVariables(this.keyfilepass)));
		retval.append("      ").append(XMLHandler.addTagValue("compression", this.compression));
		retval.append("      ").append(XMLHandler.addTagValue("proxyType", this.proxyType));
		retval.append("      ").append(XMLHandler.addTagValue("proxyHost", this.proxyHost));
		retval.append("      ").append(XMLHandler.addTagValue("proxyPort", this.proxyPort));
		retval.append("      ").append(XMLHandler.addTagValue("proxyUsername", this.proxyUsername));
		retval.append("      ").append(XMLHandler.addTagValue("proxyPassword", Encr.encryptPasswordIfNotUsingVariables(this.proxyPassword)));
		retval.append("      ").append(XMLHandler.addTagValue("createRemoteFolder", this.createRemoteFolder));
		retval.append("      ").append(XMLHandler.addTagValue("aftersftpput", getAfterSFTPPutCode(this.getAfterFTPS())));
		retval.append("      ").append(XMLHandler.addTagValue("destinationfolder", this.destinationfolder));
		retval.append("      ").append(XMLHandler.addTagValue("createdestinationfolder", this.createDestinationFolder));

		retval.append("      ").append(XMLHandler.addTagValue("successWhenNoFile", this.successWhenNoFile));

		return retval.toString();
	}

	private static String getAfterSFTPPutCode(int i)
	{
		if (i < 0 || i >= afterFTPSCode.length) {
			return afterFTPSCode[0];
		}
		return afterFTPSCode[i];
	}

	@Override
	public void loadXML(Node entrynode, List<DatabaseMeta> databases, List<SlaveServer> slaveServers, Repository rep) throws KettleXMLException
	{
		try {
			super.loadXML(entrynode, databases, slaveServers);
			this.serverName = XMLHandler.getTagValue(entrynode, "servername");
			this.serverPort = XMLHandler.getTagValue(entrynode, "serverport");
			this.userName = XMLHandler.getTagValue(entrynode, "username");
			this.password = Encr.decryptPasswordOptionallyEncrypted(XMLHandler.getTagValue(entrynode, "password"));
			this.sftpDirectory = XMLHandler.getTagValue(entrynode, "sftpdirectory");
			this.localDirectory = XMLHandler.getTagValue(entrynode, "localdirectory");
			this.wildcard = XMLHandler.getTagValue(entrynode, "wildcard");
			this.copyprevious = "Y".equalsIgnoreCase(XMLHandler.getTagValue(entrynode, "copyprevious"));
			this.addFilenameResut = "Y".equalsIgnoreCase(XMLHandler.getTagValue(entrynode, "addFilenameResut"));

			this.usekeyfilename = "Y".equalsIgnoreCase(XMLHandler.getTagValue(entrynode, "usekeyfilename"));
			this.keyfilename = XMLHandler.getTagValue(entrynode, "keyfilename");
			this.keyfilepass = Encr.decryptPasswordOptionallyEncrypted(XMLHandler.getTagValue(entrynode, "keyfilepass"));
			this.compression = XMLHandler.getTagValue(entrynode, "compression");
			this.proxyType = XMLHandler.getTagValue(entrynode, "proxyType");
			this.proxyHost = XMLHandler.getTagValue(entrynode, "proxyHost");
			this.proxyPort = XMLHandler.getTagValue(entrynode, "proxyPort");
			this.proxyUsername = XMLHandler.getTagValue(entrynode, "proxyUsername");
			this.proxyPassword = Encr.decryptPasswordOptionallyEncrypted(XMLHandler.getTagValue(entrynode, "proxyPassword"));

			this.createRemoteFolder = "Y".equalsIgnoreCase(XMLHandler.getTagValue(entrynode, "createRemoteFolder"));

			boolean remove = "Y".equalsIgnoreCase(XMLHandler.getTagValue(entrynode, "remove"));
			this.setAfterFTPS(getAfterSFTPPutByCode(Const.NVL(XMLHandler.getTagValue(entrynode, "aftersftpput"), "")));
			if (remove && this.getAfterFTPS() == AFTER_FTPSPUT_NOTHING) {
				this.setAfterFTPS(AFTER_FTPSPUT_DELETE);
			}
			this.destinationfolder = XMLHandler.getTagValue(entrynode, "destinationfolder");
			this.createDestinationFolder = "Y".equalsIgnoreCase(XMLHandler.getTagValue(entrynode, "createdestinationfolder"));
			this.successWhenNoFile = "Y".equalsIgnoreCase(XMLHandler.getTagValue(entrynode, "successWhenNoFile"));

		}
		catch (KettleXMLException xe) {
			throw new KettleXMLException("Unable to load job entry of type 'SFTPPUT' from XML node", xe);
		}
	}

	private static int getAfterSFTPPutByCode(String tt)
	{
		if (tt == null) {
			return 0;
		}

		for (int i = 0; i < afterFTPSCode.length; i++) {
			if (afterFTPSCode[i].equalsIgnoreCase(tt)) {
				return i;
			}
		}
		return 0;
	}

	public static String getAfterSFTPPutDesc(int i)
	{
		if (i < 0 || i >= afterFTPSDesc.length) {
			return afterFTPSDesc[0];
		}
		return afterFTPSDesc[i];
	}

	public static int getAfterSFTPPutByDesc(String tt)
	{
		if (tt == null) {
			return 0;
		}

		for (int i = 0; i < afterFTPSDesc.length; i++) {
			if (afterFTPSDesc[i].equalsIgnoreCase(tt)) {
				return i;
			}
		}

		// If this fails, try to match using the code.
		return getAfterSFTPPutByCode(tt);
	}

	@Override
	public void loadRep(Repository rep, ObjectId id_jobentry, List<DatabaseMeta> databases, List<SlaveServer> slaveServers) throws KettleException
	{
		try {
			this.serverName = rep.getJobEntryAttributeString(id_jobentry, "servername");
			this.serverPort = rep.getJobEntryAttributeString(id_jobentry, "serverport");

			this.userName = rep.getJobEntryAttributeString(id_jobentry, "username");
			this.password = Encr.decryptPasswordOptionallyEncrypted(rep.getJobEntryAttributeString(id_jobentry, "password"));
			this.sftpDirectory = rep.getJobEntryAttributeString(id_jobentry, "sftpdirectory");
			this.localDirectory = rep.getJobEntryAttributeString(id_jobentry, "localdirectory");
			this.wildcard = rep.getJobEntryAttributeString(id_jobentry, "wildcard");
			this.copyprevious = rep.getJobEntryAttributeBoolean(id_jobentry, "copyprevious");
			this.addFilenameResut = rep.getJobEntryAttributeBoolean(id_jobentry, "addFilenameResut");

			this.usekeyfilename = rep.getJobEntryAttributeBoolean(id_jobentry, "usekeyfilename");
			this.keyfilename = rep.getJobEntryAttributeString(id_jobentry, "keyfilename");
			this.keyfilepass = Encr.decryptPasswordOptionallyEncrypted(rep.getJobEntryAttributeString(id_jobentry, "keyfilepass"));
			this.compression = rep.getJobEntryAttributeString(id_jobentry, "compression");
			this.proxyType = rep.getJobEntryAttributeString(id_jobentry, "proxyType");
			this.proxyHost = rep.getJobEntryAttributeString(id_jobentry, "proxyHost");
			this.proxyPort = rep.getJobEntryAttributeString(id_jobentry, "proxyPort");
			this.proxyUsername = rep.getJobEntryAttributeString(id_jobentry, "proxyUsername");
			this.proxyPassword = Encr.decryptPasswordOptionallyEncrypted(rep.getJobEntryAttributeString(id_jobentry, "proxyPassword"));

			this.createRemoteFolder = rep.getJobEntryAttributeBoolean(id_jobentry, "createRemoteFolder");

			boolean remove = rep.getJobEntryAttributeBoolean(id_jobentry, "remove");
			this.setAfterFTPS(getAfterSFTPPutByCode(Const.NVL(rep.getJobEntryAttributeString(id_jobentry, "aftersftpput"), "")));
			if (remove && this.getAfterFTPS() == AFTER_FTPSPUT_NOTHING) {
				this.setAfterFTPS(AFTER_FTPSPUT_DELETE);
			}
			this.destinationfolder = rep.getJobEntryAttributeString(id_jobentry, "destinationfolder");
			this.createDestinationFolder = rep.getJobEntryAttributeBoolean(id_jobentry, "createdestinationfolder");
			this.successWhenNoFile = rep.getJobEntryAttributeBoolean(id_jobentry, "successWhenNoFile");

		}
		catch (KettleException dbe) {
			throw new KettleException("Unable to load job entry of type 'SFTPPUT' from the repository for id_jobentry=" + id_jobentry, dbe);
		}
	}

	@Override
	public void saveRep(Repository rep, ObjectId id_job) throws KettleException
	{
		try {
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "servername", this.serverName);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "serverport", this.serverPort);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "username", this.userName);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "password", Encr.encryptPasswordIfNotUsingVariables(this.password));
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "sftpdirectory", this.sftpDirectory);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "localdirectory", this.localDirectory);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "wildcard", this.wildcard);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "copyprevious", this.copyprevious);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "addFilenameResut", this.addFilenameResut);

			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "usekeyfilename", this.usekeyfilename);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "keyfilename", this.keyfilename);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "keyfilepass", Encr.encryptPasswordIfNotUsingVariables(this.keyfilepass));
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "compression", this.compression);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "proxyType", this.proxyType);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "proxyHost", this.proxyHost);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "proxyPort", this.proxyPort);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "proxyUsername", this.proxyUsername);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "proxyPassword", Encr.encryptPasswordIfNotUsingVariables(this.proxyPassword));
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "aftersftpput", getAfterSFTPPutCode(this.getAfterFTPS()));

			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "createRemoteFolder", this.createRemoteFolder);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "destinationfolder", this.destinationfolder);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "createdestinationfolder", this.createDestinationFolder);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "successWhenNoFile", this.successWhenNoFile);

		}
		catch (KettleDatabaseException dbe) {
			throw new KettleException("Unable to load job entry of type 'SFTPPUT' to the repository for id_job=" + id_job, dbe);
		}
	}

	/**
	 * @param createDestinationFolder The create destination folder flag to set.
	 */
	public void setCreateDestinationFolder(boolean createDestinationFolder)
	{
		this.createDestinationFolder = createDestinationFolder;
	}

	/**
	 * @return Returns the create destination folder flag
	 */
	public boolean isCreateDestinationFolder()
	{
		return this.createDestinationFolder;
	}

	/**
	 * @param successWhenNoFile The successWhenNoFile flag to set.
	 */
	public void setSuccessWhenNoFile(boolean successWhenNoFile)
	{
		this.successWhenNoFile = successWhenNoFile;
	}

	/**
	 * @return Returns the create successWhenNoFile folder flag
	 */
	public boolean isSuccessWhenNoFile()
	{
		return this.successWhenNoFile;
	}

	public void setDestinationFolder(String destinationfolderin)
	{
		this.destinationfolder = destinationfolderin;
	}

	public String getDestinationFolder()
	{
		return this.destinationfolder;
	}

	/**
	 * @return Returns the afterFTPS.
	 */
	public int getAfterFTPS()
	{
		return this.afterFTPS;
	}

	/**
	 * @param value The afterFTPS to set.
	 */
	public void setAfterFTPS(int value)
	{
		this.afterFTPS = value;
	}

	/**
	 * @return Returns the directory.
	 */
	public String getScpDirectory()
	{
		return this.sftpDirectory;
	}

	/**
	 * @param directory The directory to set.
	 */
	public void setScpDirectory(String directory)
	{
		this.sftpDirectory = directory;
	}

	/**
	 * @return Returns the password.
	 */
	public String getPassword()
	{
		return this.password;
	}

	/**
	 * @param password The password to set.
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return Returns the serverName.
	 */
	public String getServerName()
	{
		return this.serverName;
	}

	/**
	 * @param serverName The serverName to set.
	 */
	public void setServerName(String serverName)
	{
		this.serverName = serverName;
	}

	/**
	 * @return Returns the userName.
	 */
	public String getUserName()
	{
		return this.userName;
	}

	/**
	 * @param userName The userName to set.
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * @return Returns the wildcard.
	 */
	public String getWildcard()
	{
		return this.wildcard;
	}

	/**
	 * @param wildcard The wildcard to set.
	 */
	public void setWildcard(String wildcard)
	{
		this.wildcard = wildcard;
	}

	/**
	 * @return Returns the localdirectory.
	 */
	public String getLocalDirectory()
	{
		return this.localDirectory;
	}

	/**
	 * @param localDirectory The localDirectory to set.
	 */
	public void setLocalDirectory(String localDirectory)
	{
		this.localDirectory = localDirectory;
	}

	public boolean isCopyPrevious()
	{
		return this.copyprevious;
	}

	public void setCopyPrevious(boolean copyprevious)
	{
		this.copyprevious = copyprevious;
	}

	public boolean isAddFilenameResut()
	{
		return this.addFilenameResut;
	}

	public boolean isUseKeyFile()
	{
		return this.usekeyfilename;
	}

	public void setUseKeyFile(boolean value)
	{
		this.usekeyfilename = value;
	}

	public String getKeyFilename()
	{
		return this.keyfilename;
	}

	public void setKeyFilename(String value)
	{
		this.keyfilename = value;
	}

	public String getKeyPassPhrase()
	{
		return this.keyfilepass;
	}

	public void setKeyPassPhrase(String value)
	{
		this.keyfilepass = value;
	}

	public void setAddFilenameResut(boolean addFilenameResut)
	{
		this.addFilenameResut = addFilenameResut;
	}

	/**
	 * @return Returns the compression.
	 */
	public String getCompression()
	{
		return this.compression;
	}

	/**
	 * @param compression The compression to set.
	 */
	public void setCompression(String compression)
	{
		this.compression = compression;
	}

	public String getServerPort()
	{
		return this.serverPort;
	}

	public void setServerPort(String serverPort)
	{
		this.serverPort = serverPort;
	}

	public String getProxyType()
	{
		return this.proxyType;
	}

	public void setProxyType(String value)
	{
		this.proxyType = value;
	}

	public String getProxyHost()
	{
		return this.proxyHost;
	}

	public void setProxyHost(String value)
	{
		this.proxyHost = value;
	}

	public String getProxyPort()
	{
		return this.proxyPort;
	}

	public void setProxyPort(String value)
	{
		this.proxyPort = value;
	}

	public String getProxyUsername()
	{
		return this.proxyUsername;
	}

	public void setProxyUsername(String value)
	{
		this.proxyUsername = value;
	}

	public String getProxyPassword()
	{
		return this.proxyPassword;
	}

	public void setProxyPassword(String value)
	{
		this.proxyPassword = value;
	}

	public boolean isCreateRemoteFolder()
	{
		return this.createRemoteFolder;
	}

	public void setCreateRemoteFolder(boolean value)
	{
		this.createRemoteFolder = value;
	}

	@Override
	public Result execute(Result previousResult, int nr) throws KettleException
	{
		Result result = previousResult;
		List<RowMetaAndData> rows = result.getRows();
		result.setResult(false);

		if (this.log.isDetailed()) {
			this.logDetailed(BaseMessages.getString(PKG, "JobSFTPPUT.Log.StartJobEntry"));
		}
		ArrayList<FileObject> myFileList = new ArrayList<FileObject>();

		if (this.copyprevious) {
			if (rows.size() == 0) {
				if (this.log.isDetailed()) {
					this.logDetailed(BaseMessages.getString(PKG, "JobSFTPPUT.ArgsFromPreviousNothing"));
				}
				result.setResult(true);
				return result;
			}

			try {
				RowMetaAndData resultRow = null;
				// Copy the input row to the (command line) arguments
				for (int iteration = 0; iteration < rows.size(); iteration++) {
					resultRow = rows.get(iteration);

					// Get file names
					String file_previous = resultRow.getString(0, null);
					if (!Const.isEmpty(file_previous)) {
						FileObject file = KettleVFS.getFileObject(file_previous, this);
						if (!file.exists()) {
							this.logError(BaseMessages.getString(PKG, "JobSFTPPUT.Log.FilefromPreviousNotFound", file_previous));
						}
						else {
							myFileList.add(file);
							if (this.log.isDebug()) {
								this.logDebug(BaseMessages.getString(PKG, "JobSFTPPUT.Log.FilenameFromResult", file_previous));
							}
						}
					}
				}
			}
			catch (Exception e) {
				this.logError(BaseMessages.getString(PKG, "JobSFTPPUT.Error.ArgFromPrevious"));
				result.setNrErrors(1);
				// free resource
				myFileList = null;
				return result;
			}
		}
		SFTPClient sftpclient = null;

		// String substitution..
		String realServerName = this.environmentSubstitute(this.serverName);
		String realServerPort = this.environmentSubstitute(this.serverPort);
		String realUsername = this.environmentSubstitute(this.userName);
		String realPassword = Encr.decryptPasswordOptionallyEncrypted(this.environmentSubstitute(this.password));
		String realSftpDirString = this.environmentSubstitute(this.sftpDirectory);
		String realWildcard = this.environmentSubstitute(this.wildcard);
		String realLocalDirectory = this.environmentSubstitute(this.localDirectory);
		String realKeyFilename = null;
		String realPassPhrase = null;
		// Destination folder (Move to)
		String realDestinationFolder = this.environmentSubstitute(this.getDestinationFolder());

		try {
			// Let's perform some checks before starting

			if (this.getAfterFTPS() == AFTER_FTPSPUT_MOVE) {
				if (Const.isEmpty(realDestinationFolder)) {
					this.logError(BaseMessages.getString(PKG, "JobSSH2PUT.Log.DestinatFolderMissing"));
					result.setNrErrors(1);
					return result;
				}
				else {
					FileObject folder = null;
					try {
						folder = KettleVFS.getFileObject(realDestinationFolder, this);
						// Let's check if folder exists...
						if (!folder.exists()) {
							// Do we need to create it?
							if (this.createDestinationFolder) {
								folder.createFolder();
							}
							else {
								this.logError(BaseMessages.getString(PKG, "JobSSH2PUT.Log.DestinatFolderNotExist", realDestinationFolder));
								result.setNrErrors(1);
								return result;
							}
						}
						realDestinationFolder = KettleVFS.getFilename(folder);
					}
					catch (Exception e) {
						throw new KettleException(e);
					}
					finally {
						if (folder != null) {
							try {
								folder.close();
							}
							catch (Exception e) {
							};
						}
					}
				}
			}

			if (this.isUseKeyFile()) {
				// We must have here a private keyfilename
				realKeyFilename = this.environmentSubstitute(this.getKeyFilename());
				if (Const.isEmpty(realKeyFilename)) {
					// Error..Missing keyfile
					this.logError(BaseMessages.getString(PKG, "JobSFTP.Error.KeyFileMissing"));
					result.setNrErrors(1);
					return result;
				}
				if (!KettleVFS.fileExists(realKeyFilename)) {
					// Error.. can not reach keyfile
					this.logError(BaseMessages.getString(PKG, "JobSFTP.Error.KeyFileNotFound"));
					result.setNrErrors(1);
					return result;
				}
				realPassPhrase = this.environmentSubstitute(this.getKeyPassPhrase());
			}

			// Create sftp client to host ...
			sftpclient = new SFTPClient(InetAddress.getByName(realServerName), Const.toInt(realServerPort, 22), realUsername, realKeyFilename, realPassPhrase);
			if (this.log.isDetailed()) {
				this.logDetailed(BaseMessages.getString(PKG, "JobSFTPPUT.Log.OpenedConnection", realServerName, "" + realServerPort, realUsername));
			}

			// Set compression
			sftpclient.setCompression(this.getCompression());

			// Set proxy?
			String realProxyHost = this.environmentSubstitute(this.getProxyHost());
			if (!Const.isEmpty(realProxyHost)) {
				// Set proxy
				sftpclient.setProxy(realProxyHost, this.environmentSubstitute(this.getProxyPort()), this.environmentSubstitute(this.getProxyUsername()), this.environmentSubstitute(this.getProxyPassword()), this.getProxyType());
			}

			// login to ftp host ...
			sftpclient.login(realPassword);
			// Don't show the password in the logs, it's not good for security audits
			//logDetailed("logged in using password "+realPassword); // Logging this seems a bad idea! Oh well.

			// move to spool dir ...
			if (!Const.isEmpty(realSftpDirString)) {
				boolean existfolder = sftpclient.folderExists(realSftpDirString);
				if (!existfolder) {
					if (!this.isCreateRemoteFolder()) {
						throw new KettleException(BaseMessages.getString(PKG, "JobSFTPPUT.Error.CanNotFindRemoteFolder", realSftpDirString));
					}
					if (this.log.isDetailed()) {
						this.logDetailed(BaseMessages.getString(PKG, "JobSFTPPUT.Error.CanNotFindRemoteFolder", realSftpDirString));
					}

					// Let's create folder
					sftpclient.createFolder(realSftpDirString);
					if (this.log.isDetailed()) {
						this.logDetailed(BaseMessages.getString(PKG, "JobSFTPPUT.Log.RemoteFolderCreated", realSftpDirString));
					}
				}
				sftpclient.chdir(realSftpDirString);
				if (this.log.isDetailed()) {
					this.logDetailed(BaseMessages.getString(PKG, "JobSFTPPUT.Log.ChangedDirectory", realSftpDirString));
				}
			} // end if

			if (!this.copyprevious) {
				// Get all the files in the local directory...
				myFileList = new ArrayList<FileObject>();

				FileObject localFiles = KettleVFS.getFileObject(realLocalDirectory, this);
				FileObject[] children = localFiles.getChildren();
				if (children != null) {
					for (FileObject element : children) {
						// Get filename of file or directory
						if (element.getType().equals(FileType.FILE)) {
							// myFileList.add(children[i].getAbsolutePath());
							myFileList.add(element);
						}
					} // end for
				}
			}

			if (myFileList == null || myFileList.size() == 0) {
				if (this.isSuccessWhenNoFile()) {
					// Just warn user
					if (this.isBasic()) {
						this.logBasic(BaseMessages.getString(PKG, "JobSFTPPUT.Error.NoFileToSend"));
					}
				}
				else {
					// Fail
					this.logError(BaseMessages.getString(PKG, "JobSFTPPUT.Error.NoFileToSend"));
					result.setNrErrors(1);
					return result;
				}
			}

			if (this.log.isDetailed()) {
				this.logDetailed(BaseMessages.getString(PKG, "JobSFTPPUT.Log.RowsFromPreviousResult", myFileList.size()));
			}

			Pattern pattern = null;
			if (!this.copyprevious) {
				if (!Const.isEmpty(realWildcard)) {
					pattern = Pattern.compile(realWildcard);
				}
			}

			// Get the files in the list and execute sftp.put() for each file
			Iterator<FileObject> it = myFileList.iterator();
			while (it.hasNext() && !this.parentJob.isStopped()) {
				FileObject myFile = it.next();
				try {
					String localFilename = myFile.toString();
					String destinationFilename = myFile.getName().getBaseName();
					boolean getIt = true;

					// First see if the file matches the regular expression!
					if (pattern != null) {
						Matcher matcher = pattern.matcher(destinationFilename);
						getIt = matcher.matches();
					}

					if (getIt) {
						if (this.log.isDebug()) {
							this.logDebug(BaseMessages.getString(PKG, "JobSFTPPUT.Log.PuttingFile", localFilename, realSftpDirString));
						}

						sftpclient.put(myFile, destinationFilename);

						if (this.log.isDetailed()) {
							this.logDetailed(BaseMessages.getString(PKG, "JobSFTPPUT.Log.TransferedFile", localFilename));
						}

						// We successfully uploaded the file
						// what's next ...
						switch (this.getAfterFTPS()) {
							case AFTER_FTPSPUT_DELETE:
								myFile.delete();
								if (this.log.isDetailed()) {
									this.logDetailed(BaseMessages.getString(PKG, "JobSFTPPUT.Log.DeletedFile", localFilename));
								}
								break;
							case AFTER_FTPSPUT_MOVE:
								FileObject destination = null;
								try {
									destination = KettleVFS.getFileObject(realDestinationFolder + Const.FILE_SEPARATOR + myFile.getName().getBaseName(), this);
									myFile.moveTo(destination);
									if (this.log.isDetailed()) {
										this.logDetailed(BaseMessages.getString(PKG, "JobSFTPPUT.Log.FileMoved", myFile, destination));
									}
								}
								finally {
									if (destination != null) {
										destination.close();
									}
								}
								break;
							default:
								if (this.addFilenameResut) {
									// Add to the result files...
									ResultFile resultFile = new ResultFile(ResultFile.FILE_TYPE_GENERAL, myFile, this.parentJob.getJobname(), this.toString());
									result.getResultFiles().put(resultFile.getFile().toString(), resultFile);
									if (this.log.isDetailed()) {
										this.logDetailed(BaseMessages.getString(PKG, "JobSFTPPUT.Log.FilenameAddedToResultFilenames", localFilename));
									}
								}
								break;
						}

					}
				}
				finally {
					if (myFile != null) {
						myFile.close();
					}
				}
			} // end for

			result.setResult(true);
			// JKU: no idea if this is needed...!
			// result.setNrFilesRetrieved(filesRetrieved);
		} // end try
		catch (Exception e) {
			result.setNrErrors(1);
			this.logError(BaseMessages.getString(PKG, "JobSFTPPUT.Exception", e.getMessage()));
			this.logError(Const.getStackTracker(e));
		}
		finally {
			// close connection, if possible
			try {
				if (sftpclient != null) {
					sftpclient.disconnect();
				}
			}
			catch (Exception e) {
				// just ignore this, makes no big difference
			} // end catch
			myFileList = null;
		} // end finally

		return result;
	} // JKU: end function execute()

	@Override
	public boolean evaluates()
	{
		return true;
	}

	@Override
	public List<ResourceReference> getResourceDependencies(JobMeta jobMeta)
	{
		List<ResourceReference> references = super.getResourceDependencies(jobMeta);
		if (!Const.isEmpty(this.serverName)) {
			String realServerName = jobMeta.environmentSubstitute(this.serverName);
			ResourceReference reference = new ResourceReference(this);
			reference.getEntries().add(new ResourceEntry(realServerName, ResourceType.SERVER));
			references.add(reference);
		}
		return references;
	}

	@Override
	public void check(List<CheckResultInterface> remarks, JobMeta jobMeta)
	{
		andValidator().validate(this, "serverName", remarks, putValidators(notBlankValidator())); //$NON-NLS-1$
		andValidator().validate(this, "localDirectory", remarks, putValidators(notBlankValidator(), fileExistsValidator())); //$NON-NLS-1$
		andValidator().validate(this, "userName", remarks, putValidators(notBlankValidator())); //$NON-NLS-1$
		andValidator().validate(this, "password", remarks, putValidators(notNullValidator())); //$NON-NLS-1$
		andValidator().validate(this, "serverPort", remarks, putValidators(integerValidator())); //$NON-NLS-1$
	}

}
