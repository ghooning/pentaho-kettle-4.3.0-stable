/*******************************************************************************
 *
 * Pentaho Data Integration
 *
 * Copyright (C) 2002-2012 by Pentaho : http://www.pentaho.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/

package org.pentaho.di.job.entries.sftp;

import static org.pentaho.di.job.entry.validator.AbstractFileValidator.putVariableSpace;
import static org.pentaho.di.job.entry.validator.AndValidator.putValidators;
import static org.pentaho.di.job.entry.validator.JobEntryValidatorUtils.andValidator;
import static org.pentaho.di.job.entry.validator.JobEntryValidatorUtils.fileExistsValidator;
import static org.pentaho.di.job.entry.validator.JobEntryValidatorUtils.integerValidator;
import static org.pentaho.di.job.entry.validator.JobEntryValidatorUtils.notBlankValidator;
import static org.pentaho.di.job.entry.validator.JobEntryValidatorUtils.notNullValidator;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.vfs.FileObject;
import org.pentaho.di.cluster.SlaveServer;
import org.pentaho.di.core.CheckResultInterface;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.Result;
import org.pentaho.di.core.ResultFile;
import org.pentaho.di.core.RowMetaAndData;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.encryption.Encr;
import org.pentaho.di.core.exception.KettleDatabaseException;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.core.vfs.KettleVFS;
import org.pentaho.di.core.xml.XMLHandler;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.job.JobMeta;
import org.pentaho.di.job.entry.JobEntryBase;
import org.pentaho.di.job.entry.JobEntryInterface;
import org.pentaho.di.job.entry.validator.ValidatorContext;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.resource.ResourceEntry;
import org.pentaho.di.resource.ResourceEntry.ResourceType;
import org.pentaho.di.resource.ResourceReference;
import org.w3c.dom.Node;

/**
 * This defines a SFTP job entry.
 *
 * @author Matt
 * @since 05-11-2003
 *
 */
public class JobEntrySFTP extends JobEntryBase implements Cloneable, JobEntryInterface
{
	private static Class<?> PKG = JobEntrySFTP.class; // for i18n purposes, needed by Translator2!!   $NON-NLS-1$

	private static final int DEFAULT_PORT = 22;

	private String serverName;

	private String serverPort;

	private String userName;

	private String password;

	private String sftpDirectory;

	private String targetDirectory;

	private String wildcard;

	private boolean remove;

	private boolean isaddresult;

	private boolean createtargetfolder;

	private boolean copyprevious;

	private boolean usekeyfilename;

	private String keyfilename;

	private String keyfilepass;

	private String compression;

	// proxy
	private String proxyType;

	private String proxyHost;

	private String proxyPort;

	private String proxyUsername;

	private String proxyPassword;

	public JobEntrySFTP(String n)
	{
		super(n, "");
		this.serverName = null;
		this.serverPort = "22";
		this.isaddresult = true;
		this.createtargetfolder = false;
		this.copyprevious = false;
		this.usekeyfilename = false;
		this.keyfilename = null;
		this.keyfilepass = null;
		this.compression = "none";
		this.proxyType = null;
		this.proxyHost = null;
		this.proxyPort = null;
		this.proxyUsername = null;
		this.proxyPassword = null;
		this.setID(-1L);
	}

	public JobEntrySFTP()
	{
		this("");
	}

	@Override
	public Object clone()
	{
		JobEntrySFTP je = (JobEntrySFTP) super.clone();
		return je;
	}

	@Override
	public String getXML()
	{
		StringBuffer retval = new StringBuffer(200);

		retval.append(super.getXML());

		retval.append("      ").append(XMLHandler.addTagValue("servername", this.serverName));
		retval.append("      ").append(XMLHandler.addTagValue("serverport", this.serverPort));
		retval.append("      ").append(XMLHandler.addTagValue("username", this.userName));
		retval.append("      ").append(XMLHandler.addTagValue("password", Encr.encryptPasswordIfNotUsingVariables(this.getPassword())));
		retval.append("      ").append(XMLHandler.addTagValue("sftpdirectory", this.sftpDirectory));
		retval.append("      ").append(XMLHandler.addTagValue("targetdirectory", this.targetDirectory));
		retval.append("      ").append(XMLHandler.addTagValue("wildcard", this.wildcard));
		retval.append("      ").append(XMLHandler.addTagValue("remove", this.remove));
		retval.append("      ").append(XMLHandler.addTagValue("isaddresult", this.isaddresult));
		retval.append("      ").append(XMLHandler.addTagValue("createtargetfolder", this.createtargetfolder));
		retval.append("      ").append(XMLHandler.addTagValue("copyprevious", this.copyprevious));

		retval.append("      ").append(XMLHandler.addTagValue("usekeyfilename", this.usekeyfilename));
		retval.append("      ").append(XMLHandler.addTagValue("keyfilename", this.keyfilename));
		retval.append("      ").append(XMLHandler.addTagValue("keyfilepass", Encr.encryptPasswordIfNotUsingVariables(this.keyfilepass)));
		retval.append("      ").append(XMLHandler.addTagValue("compression", this.compression));

		retval.append("      ").append(XMLHandler.addTagValue("proxyType", this.proxyType));
		retval.append("      ").append(XMLHandler.addTagValue("proxyHost", this.proxyHost));
		retval.append("      ").append(XMLHandler.addTagValue("proxyPort", this.proxyPort));
		retval.append("      ").append(XMLHandler.addTagValue("proxyUsername", this.proxyUsername));
		retval.append("      ").append(XMLHandler.addTagValue("proxyPassword", Encr.encryptPasswordIfNotUsingVariables(this.proxyPassword)));

		return retval.toString();
	}

	@Override
	public void loadXML(Node entrynode, List<DatabaseMeta> databases, List<SlaveServer> slaveServers, Repository rep) throws KettleXMLException
	{
		try {
			super.loadXML(entrynode, databases, slaveServers);
			this.serverName = XMLHandler.getTagValue(entrynode, "servername");
			this.serverPort = XMLHandler.getTagValue(entrynode, "serverport");
			this.userName = XMLHandler.getTagValue(entrynode, "username");
			this.password = Encr.decryptPasswordOptionallyEncrypted(XMLHandler.getTagValue(entrynode, "password"));
			this.sftpDirectory = XMLHandler.getTagValue(entrynode, "sftpdirectory");
			this.targetDirectory = XMLHandler.getTagValue(entrynode, "targetdirectory");
			this.wildcard = XMLHandler.getTagValue(entrynode, "wildcard");
			this.remove = "Y".equalsIgnoreCase(XMLHandler.getTagValue(entrynode, "remove"));

			String addresult = XMLHandler.getTagValue(entrynode, "isaddresult");

			if (Const.isEmpty(addresult)) {
				this.isaddresult = true;
			}
			else {
				this.isaddresult = "Y".equalsIgnoreCase(addresult);
			}

			this.createtargetfolder = "Y".equalsIgnoreCase(XMLHandler.getTagValue(entrynode, "createtargetfolder"));
			this.copyprevious = "Y".equalsIgnoreCase(XMLHandler.getTagValue(entrynode, "copyprevious"));

			this.usekeyfilename = "Y".equalsIgnoreCase(XMLHandler.getTagValue(entrynode, "usekeyfilename"));
			this.keyfilename = XMLHandler.getTagValue(entrynode, "keyfilename");
			this.keyfilepass = Encr.decryptPasswordOptionallyEncrypted(XMLHandler.getTagValue(entrynode, "keyfilepass"));
			this.compression = XMLHandler.getTagValue(entrynode, "compression");

			this.proxyType = XMLHandler.getTagValue(entrynode, "proxyType");
			this.proxyHost = XMLHandler.getTagValue(entrynode, "proxyHost");
			this.proxyPort = XMLHandler.getTagValue(entrynode, "proxyPort");
			this.proxyUsername = XMLHandler.getTagValue(entrynode, "proxyUsername");
			this.proxyPassword = Encr.decryptPasswordOptionallyEncrypted(XMLHandler.getTagValue(entrynode, "proxyPassword"));
		}
		catch (KettleXMLException xe) {
			throw new KettleXMLException("Unable to load job entry of type 'SFTP' from XML node", xe);
		}
	}

	@Override
	public void loadRep(Repository rep, ObjectId id_jobentry, List<DatabaseMeta> databases, List<SlaveServer> slaveServers) throws KettleException
	{
		try {
			this.serverName = rep.getJobEntryAttributeString(id_jobentry, "servername");
			this.serverPort = rep.getJobEntryAttributeString(id_jobentry, "serverport");

			this.userName = rep.getJobEntryAttributeString(id_jobentry, "username");
			this.password = Encr.decryptPasswordOptionallyEncrypted(rep.getJobEntryAttributeString(id_jobentry, "password"));

			this.sftpDirectory = rep.getJobEntryAttributeString(id_jobentry, "sftpdirectory");
			this.targetDirectory = rep.getJobEntryAttributeString(id_jobentry, "targetdirectory");
			this.wildcard = rep.getJobEntryAttributeString(id_jobentry, "wildcard");
			this.remove = rep.getJobEntryAttributeBoolean(id_jobentry, "remove");

			String addToResult = rep.getStepAttributeString(id_jobentry, "add_to_result_filenames");
			if (Const.isEmpty(addToResult)) {
				this.isaddresult = true;
			}
			else {
				this.isaddresult = rep.getStepAttributeBoolean(id_jobentry, "add_to_result_filenames");
			}

			this.createtargetfolder = rep.getJobEntryAttributeBoolean(id_jobentry, "createtargetfolder");
			this.copyprevious = rep.getJobEntryAttributeBoolean(id_jobentry, "copyprevious");

			this.usekeyfilename = rep.getJobEntryAttributeBoolean(id_jobentry, "usekeyfilename");
			this.keyfilename = rep.getJobEntryAttributeString(id_jobentry, "keyfilename");
			this.keyfilepass = Encr.decryptPasswordOptionallyEncrypted(rep.getJobEntryAttributeString(id_jobentry, "keyfilepass"));
			this.compression = rep.getJobEntryAttributeString(id_jobentry, "compression");

			this.proxyType = rep.getJobEntryAttributeString(id_jobentry, "proxyType");
			this.proxyHost = rep.getJobEntryAttributeString(id_jobentry, "proxyHost");
			this.proxyPort = rep.getJobEntryAttributeString(id_jobentry, "proxyPort");
			this.proxyUsername = rep.getJobEntryAttributeString(id_jobentry, "proxyUsername");
			this.proxyPassword = Encr.decryptPasswordOptionallyEncrypted(rep.getJobEntryAttributeString(id_jobentry, "proxyPassword"));

		}
		catch (KettleException dbe) {
			throw new KettleException("Unable to load job entry of type 'SFTP' from the repository for id_jobentry=" + id_jobentry, dbe);
		}
	}

	@Override
	public void saveRep(Repository rep, ObjectId id_job) throws KettleException
	{
		try {
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "servername", this.serverName);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "serverport", this.serverPort);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "username", this.userName);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "password", Encr.encryptPasswordIfNotUsingVariables(this.password)); //$NON-NLS-1$
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "sftpdirectory", this.sftpDirectory);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "targetdirectory", this.targetDirectory);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "wildcard", this.wildcard);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "remove", this.remove);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "isaddresult", this.isaddresult);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "createtargetfolder", this.createtargetfolder);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "copyprevious", this.copyprevious);

			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "usekeyfilename", this.usekeyfilename);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "keyfilename", this.keyfilename);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "keyfilepass", Encr.encryptPasswordIfNotUsingVariables(this.keyfilepass));
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "compression", this.compression);

			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "proxyType", this.proxyType);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "proxyHost", this.proxyHost);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "proxyPort", this.proxyPort);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "proxyUsername", this.proxyUsername);
			rep.saveJobEntryAttribute(id_job, this.getObjectId(), "proxyPassword", Encr.encryptPasswordIfNotUsingVariables(this.proxyPassword));
		}
		catch (KettleDatabaseException dbe) {
			throw new KettleException("Unable to save job entry of type 'SFTP' to the repository for id_job=" + id_job, dbe);
		}
	}

	/**
	 * @return Returns the directory.
	 */
	public String getScpDirectory()
	{
		return this.sftpDirectory;
	}

	/**
	 * @param directory The directory to set.
	 */
	public void setScpDirectory(String directory)
	{
		this.sftpDirectory = directory;
	}

	/**
	 * @return Returns the password.
	 */
	public String getPassword()
	{
		return this.password;
	}

	/**
	 * @param password The password to set.
	 */
	public void setPassword(String password)
	{
		this.password = password;
	}

	/**
	 * @return Returns the compression.
	 */
	public String getCompression()
	{
		return this.compression;
	}

	/**
	 * @param compression The compression to set.
	 */
	public void setCompression(String compression)
	{
		this.compression = compression;
	}

	/**
	 * @return Returns the serverName.
	 */
	public String getServerName()
	{
		return this.serverName;
	}

	/**
	 * @param serverName The serverName to set.
	 */
	public void setServerName(String serverName)
	{
		this.serverName = serverName;
	}

	/**
	 * @return Returns the userName.
	 */
	public String getUserName()
	{
		return this.userName;
	}

	/**
	 * @param userName The userName to set.
	 */
	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	/**
	 * @return Returns the wildcard.
	 */
	public String getWildcard()
	{
		return this.wildcard;
	}

	/**
	 * @param wildcard The wildcard to set.
	 */
	public void setWildcard(String wildcard)
	{
		this.wildcard = wildcard;
	}

	public void setAddToResult(boolean isaddresultin)
	{
		this.isaddresult = isaddresultin;
	}

	public boolean isAddToResult()
	{
		return this.isaddresult;
	}

	/**
	 * @return Returns the targetDirectory.
	 */
	public String getTargetDirectory()
	{
		return this.targetDirectory;
	}

	public void setcreateTargetFolder(boolean createtargetfolder)
	{
		this.createtargetfolder = createtargetfolder;
	}

	public boolean iscreateTargetFolder()
	{
		return this.createtargetfolder;
	}

	public boolean isCopyPrevious()
	{
		return this.copyprevious;
	}

	public void setCopyPrevious(boolean copyprevious)
	{
		this.copyprevious = copyprevious;
	}

	/**
	 * @param targetDirectory The targetDirectory to set.
	 */
	public void setTargetDirectory(String targetDirectory)
	{
		this.targetDirectory = targetDirectory;
	}

	/**
	 * @param remove The remove to set.
	 */
	public void setRemove(boolean remove)
	{
		this.remove = remove;
	}

	/**
	 * @return Returns the remove.
	 */
	public boolean getRemove()
	{
		return this.remove;
	}

	public String getServerPort()
	{
		return this.serverPort;
	}

	public void setServerPort(String serverPort)
	{
		this.serverPort = serverPort;
	}

	public boolean isUseKeyFile()
	{
		return this.usekeyfilename;
	}

	public void setUseKeyFile(boolean value)
	{
		this.usekeyfilename = value;
	}

	public String getKeyFilename()
	{
		return this.keyfilename;
	}

	public void setKeyFilename(String value)
	{
		this.keyfilename = value;
	}

	public String getKeyPassPhrase()
	{
		return this.keyfilepass;
	}

	public void setKeyPassPhrase(String value)
	{
		this.keyfilepass = value;
	}

	public String getProxyType()
	{
		return this.proxyType;
	}

	public void setProxyType(String value)
	{
		this.proxyType = value;
	}

	public String getProxyHost()
	{
		return this.proxyHost;
	}

	public void setProxyHost(String value)
	{
		this.proxyHost = value;
	}

	public String getProxyPort()
	{
		return this.proxyPort;
	}

	public void setProxyPort(String value)
	{
		this.proxyPort = value;
	}

	public String getProxyUsername()
	{
		return this.proxyUsername;
	}

	public void setProxyUsername(String value)
	{
		this.proxyUsername = value;
	}

	public String getProxyPassword()
	{
		return this.proxyPassword;
	}

	public void setProxyPassword(String value)
	{
		this.proxyPassword = value;
	}

	@Override
	public Result execute(Result previousResult, int nr)
	{
		Result result = previousResult;
		List<RowMetaAndData> rows = result.getRows();
		RowMetaAndData resultRow = null;

		result.setResult(false);
		long filesRetrieved = 0;

		if (this.log.isDetailed()) {
			this.logDetailed(BaseMessages.getString(PKG, "JobSFTP.Log.StartJobEntry"));
		}
		HashSet<String> list_previous_filenames = new HashSet<String>();

		if (this.copyprevious) {
			if (rows.size() == 0) {
				if (this.log.isDetailed()) {
					this.logDetailed(BaseMessages.getString(PKG, "JobSFTP.ArgsFromPreviousNothing"));
				}
				result.setResult(true);
				return result;
			}
			try {

				// Copy the input row to the (command line) arguments
				for (int iteration = 0; iteration < rows.size(); iteration++) {
					resultRow = rows.get(iteration);

					// Get file names
					String file_previous = resultRow.getString(0, null);
					if (!Const.isEmpty(file_previous)) {
						list_previous_filenames.add(file_previous);
						if (this.log.isDebug()) {
							this.logDebug(BaseMessages.getString(PKG, "JobSFTP.Log.FilenameFromResult", file_previous));
						}
					}
				}
			}
			catch (Exception e) {
				this.logError(BaseMessages.getString(PKG, "JobSFTP.Error.ArgFromPrevious"));
				result.setNrErrors(1);
				return result;
			}
		}

		SFTPClient sftpclient = null;

		// String substitution..
		String realServerName = this.environmentSubstitute(this.serverName);
		String realServerPort = this.environmentSubstitute(this.serverPort);
		String realUsername = this.environmentSubstitute(this.userName);
		String realPassword = Encr.decryptPasswordOptionallyEncrypted(this.environmentSubstitute(this.password));
		String realSftpDirString = this.environmentSubstitute(this.sftpDirectory);
		String realWildcard = this.environmentSubstitute(this.wildcard);
		String realTargetDirectory = this.environmentSubstitute(this.targetDirectory);
		String realKeyFilename = null;
		String realPassPhrase = null;
		FileObject TargetFolder = null;

		try {
			// Let's perform some checks before starting
			if (this.isUseKeyFile()) {
				// We must have here a private keyfilename
				realKeyFilename = this.environmentSubstitute(this.getKeyFilename());
				if (Const.isEmpty(realKeyFilename)) {
					// Error..Missing keyfile
					this.logError(BaseMessages.getString(PKG, "JobSFTP.Error.KeyFileMissing"));
					result.setNrErrors(1);
					return result;
				}
				if (!KettleVFS.fileExists(realKeyFilename)) {
					// Error.. can not reach keyfile
					this.logError(BaseMessages.getString(PKG, "JobSFTP.Error.KeyFileNotFound", realKeyFilename));
					result.setNrErrors(1);
					return result;
				}
				realPassPhrase = this.environmentSubstitute(this.getKeyPassPhrase());
			}

			if (!Const.isEmpty(realTargetDirectory)) {
				TargetFolder = KettleVFS.getFileObject(realTargetDirectory, this);
				boolean TargetFolderExists = TargetFolder.exists();
				if (TargetFolderExists) {
					if (this.log.isDetailed()) {
						this.logDetailed(BaseMessages.getString(PKG, "JobSFTP.Log.TargetFolderExists", realTargetDirectory));
					}
				}
				else {
					this.logError(BaseMessages.getString(PKG, "JobSFTP.Error.TargetFolderNotExists", realTargetDirectory));
					if (!this.createtargetfolder) {
						// Error..Target folder can not be found !
						result.setNrErrors(1);
						return result;
					}
					else {
						// create target folder
						TargetFolder.createFolder();
						if (this.log.isDetailed()) {
							this.logDetailed(BaseMessages.getString(PKG, "JobSFTP.Log.TargetFolderCreated", realTargetDirectory));
						}
					}
				}
			}

			if (TargetFolder != null) {
				TargetFolder.close();
				TargetFolder = null;
			}

			// Create sftp client to host ...
			sftpclient = new SFTPClient(InetAddress.getByName(realServerName), Const.toInt(realServerPort, DEFAULT_PORT), realUsername, realKeyFilename, realPassPhrase);
			if (this.log.isDetailed()) {
				this.logDetailed(BaseMessages.getString(PKG, "JobSFTP.Log.OpenedConnection", realServerName, realServerPort, realUsername));
			}

			// Set compression
			sftpclient.setCompression(this.getCompression());

			// Set proxy?
			String realProxyHost = this.environmentSubstitute(this.getProxyHost());
			if (!Const.isEmpty(realProxyHost)) {
				// Set proxy
				sftpclient.setProxy(realProxyHost, this.environmentSubstitute(this.getProxyPort()), this.environmentSubstitute(this.getProxyUsername()), this.environmentSubstitute(this.getProxyPassword()), this.getProxyType());
			}

			// login to ftp host ...
			sftpclient.login(realPassword);
			// Passwords should not appear in log files.
			//logDetailed("logged in using password "+realPassword); // Logging this seems a bad idea! Oh well.

			// move to spool dir ...
			if (!Const.isEmpty(realSftpDirString)) {
				try {
					sftpclient.chdir(realSftpDirString);
				}
				catch (Exception e) {
					this.logError(BaseMessages.getString(PKG, "JobSFTP.Error.CanNotFindRemoteFolder", realSftpDirString));
					throw new Exception(e);
				}
				if (this.log.isDetailed()) {
					this.logDetailed(BaseMessages.getString(PKG, "JobSFTP.Log.ChangedDirectory", realSftpDirString));
				}
			}
			Pattern pattern = null;
			// Get all the files in the current directory...
			String[] filelist = sftpclient.dir();
			if (filelist == null) {
				// Nothing was found !!! exit
				result.setResult(true);
				if (this.log.isDetailed()) {
					this.logDetailed(BaseMessages.getString(PKG, "JobSFTP.Log.Found", "" + 0));
				}
				return result;
			}
			if (this.log.isDetailed()) {
				this.logDetailed(BaseMessages.getString(PKG, "JobSFTP.Log.Found", "" + filelist.length));
			}

			if (!this.copyprevious) {
				if (!Const.isEmpty(realWildcard)) {
					pattern = Pattern.compile(realWildcard);
				}
			}

			// Get the files in the list...
			for (int i = 0; i < filelist.length && !this.parentJob.isStopped(); i++) {
				boolean getIt = true;

				if (this.copyprevious) {
					// filenames list is send by previous job entry
					// download if the current file is in this list
					getIt = list_previous_filenames.contains(filelist[i]);
				}
				else {
					// download files
					// but before see if the file matches the regular expression!
					if (pattern != null) {
						Matcher matcher = pattern.matcher(filelist[i]);
						getIt = matcher.matches();
					}
				}

				if (getIt) {
					if (this.log.isDebug()) {
						this.logDebug(BaseMessages.getString(PKG, "JobSFTP.Log.GettingFiles", filelist[i], realTargetDirectory));
					}

					String targetFilename = realTargetDirectory + Const.FILE_SEPARATOR + filelist[i];
					sftpclient.get(targetFilename, filelist[i]);
					filesRetrieved++;

					if (this.isaddresult) {
						// Add to the result files...
						ResultFile resultFile = new ResultFile(ResultFile.FILE_TYPE_GENERAL, KettleVFS.getFileObject(targetFilename, this), this.parentJob.getJobname(), this.toString());
						result.getResultFiles().put(resultFile.getFile().toString(), resultFile);
						if (this.log.isDetailed()) {
							this.logDetailed(BaseMessages.getString(PKG, "JobSFTP.Log.FilenameAddedToResultFilenames", filelist[i]));
						}
					}
					if (this.log.isDetailed()) {
						this.logDetailed(BaseMessages.getString(PKG, "JobSFTP.Log.TransferedFile", filelist[i]));
					}

					// Delete the file if this is needed!
					if (this.remove) {
						sftpclient.delete(filelist[i]);
						if (this.log.isDetailed()) {
							this.logDetailed(BaseMessages.getString(PKG, "JobSFTP.Log.DeletedFile", filelist[i]));
						}
					}
				}
			}

			result.setResult(true);
			result.setNrFilesRetrieved(filesRetrieved);
		}
		catch (Exception e) {
			result.setNrErrors(1);
			this.logError(BaseMessages.getString(PKG, "JobSFTP.Error.GettingFiles", e.getMessage()));
			this.logError(Const.getStackTracker(e));
		}
		finally {
			// close connection, if possible
			try {
				if (sftpclient != null) {
					sftpclient.disconnect();
				}
			}
			catch (Exception e) {
				// just ignore this, makes no big difference
			}

			try {
				if (TargetFolder != null) {
					TargetFolder.close();
					TargetFolder = null;
				}
				if (list_previous_filenames != null) {
					list_previous_filenames = null;
				}
			}
			catch (Exception e) {
			}

		}

		return result;
	}

	@Override
	public boolean evaluates()
	{
		return true;
	}

	@Override
	public List<ResourceReference> getResourceDependencies(JobMeta jobMeta)
	{
		List<ResourceReference> references = super.getResourceDependencies(jobMeta);
		if (!Const.isEmpty(this.serverName)) {
			String realServerName = jobMeta.environmentSubstitute(this.serverName);
			ResourceReference reference = new ResourceReference(this);
			reference.getEntries().add(new ResourceEntry(realServerName, ResourceType.SERVER));
			references.add(reference);
		}
		return references;
	}

	@Override
	public void check(List<CheckResultInterface> remarks, JobMeta jobMeta)
	{
		andValidator().validate(this, "serverName", remarks, putValidators(notBlankValidator())); //$NON-NLS-1$

		ValidatorContext ctx = new ValidatorContext();
		putVariableSpace(ctx, this.getVariables());
		putValidators(ctx, notBlankValidator(), fileExistsValidator());
		andValidator().validate(this, "targetDirectory", remarks, ctx);//$NON-NLS-1$

		andValidator().validate(this, "userName", remarks, putValidators(notBlankValidator())); //$NON-NLS-1$
		andValidator().validate(this, "password", remarks, putValidators(notNullValidator())); //$NON-NLS-1$
		andValidator().validate(this, "serverPort", remarks, putValidators(integerValidator())); //$NON-NLS-1$
	}

	public static void main(String[] args)
	{
		List<CheckResultInterface> remarks = new ArrayList<CheckResultInterface>();
		new JobEntrySFTP().check(remarks, null);
		System.out.printf("Remarks: %s\n", remarks);
	}

}
