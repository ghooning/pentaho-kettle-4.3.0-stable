/*******************************************************************************
 *
 * Pentaho Data Integration
 *
 * Copyright (C) 2002-2012 by Pentaho : http://www.pentaho.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/

package org.pentaho.di.ui.job.entries.evaluatetablecontent;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.Props;
import org.pentaho.di.core.database.Database;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.job.JobMeta;
import org.pentaho.di.job.entries.evaluatetablecontent.JobEntryEvalTableContent;
import org.pentaho.di.job.entry.JobEntryDialogInterface;
import org.pentaho.di.job.entry.JobEntryInterface;
import org.pentaho.di.repository.Repository;
import org.pentaho.di.ui.core.database.dialog.DatabaseExplorerDialog;
import org.pentaho.di.ui.core.gui.WindowProperty;
import org.pentaho.di.ui.core.widget.StyledTextComp;
import org.pentaho.di.ui.core.widget.TextVar;
import org.pentaho.di.ui.job.dialog.JobDialog;
import org.pentaho.di.ui.job.entry.JobEntryDialog;
import org.pentaho.di.ui.trans.step.BaseStepDialog;
import org.pentaho.di.ui.trans.steps.tableinput.SQLValuesHighlight;

/**
 * This dialog allows you to edit the Table content evaluation job entry settings. (select the connection and
 * the table to evaluate) 
 * 
 * @author Samatar
 * @since 22-07-2008
 */
public class JobEntryEvalTableContentDialog extends JobEntryDialog implements JobEntryDialogInterface
{
	private static Class<?> PKG = JobEntryEvalTableContent.class; // for i18n purposes, needed by Translator2!!   $NON-NLS-1$

	private Button wbTable, wbSQLTable;

	private Label wlName;

	private Text wName;

	private FormData fdlName, fdName;

	private CCombo wConnection;

	private Button wOK, wCancel;

	private Listener lsOK, lsCancel, lsbSQLTable;

	private JobEntryEvalTableContent jobEntry;

	private Shell shell;

	private SelectionAdapter lsDef;

	private boolean changed;

	private Label wlUseSubs;

	private Button wUseSubs;

	private FormData fdlUseSubs, fdUseSubs;

	private Label wlClearResultList;

	private Button wClearResultList;

	private FormData fdlClearResultList, fdClearResultList;

	private Label wlAddRowsToResult;

	private Button wAddRowsToResult;

	private FormData fdlAddRowsToResult, fdAddRowsToResult;

	private Label wlcustomSQL;

	private Button wcustomSQL;

	private FormData fdlcustomSQL, fdcustomSQL;

	private FormData fdlSQL, fdSQL;

	private Label wlSQL;

	private StyledTextComp wSQL;

	private Label wlPosition;

	private FormData fdlPosition;

	private Group wSuccessGroup;

	private FormData fdSuccessGroup;

	// Schema name
	private Label wlSchemaname;

	private TextVar wSchemaname;

	private FormData fdlSchemaname, fdSchemaname;

	private Label wlTablename;

	private TextVar wTablename;

	private FormData fdlTablename, fdTablename;

	private Group wCustomGroup;

	private FormData fdCustomGroup;

	private Label wlSuccessCondition;

	private CCombo wSuccessCondition;

	private FormData fdlSuccessCondition, fdSuccessCondition;

	private Label wlLimit;

	private TextVar wLimit;

	private FormData fdlLimit, fdLimit;

	public JobEntryEvalTableContentDialog(Shell parent, JobEntryInterface jobEntryInt, Repository rep, JobMeta jobMeta)
	{
		super(parent, jobEntryInt, rep, jobMeta);
		this.jobEntry = (JobEntryEvalTableContent) jobEntryInt;
		if (this.jobEntry.getName() == null) {
			this.jobEntry.setName(BaseMessages.getString(PKG, "JobEntryEvalTableContent.Name.Default"));
		}
	}

	@Override
	public JobEntryInterface open()
	{
		Shell parent = this.getParent();
		Display display = parent.getDisplay();

		this.shell = new Shell(parent, this.props.getJobsDialogStyle());
		this.props.setLook(this.shell);
		JobDialog.setShellImage(this.shell, this.jobEntry);

		ModifyListener lsMod = new ModifyListener()
		{
			@Override
			public void modifyText(ModifyEvent e)
			{
				JobEntryEvalTableContentDialog.this.jobEntry.setChanged();
			}
		};
		this.changed = this.jobEntry.hasChanged();

		FormLayout formLayout = new FormLayout();
		formLayout.marginWidth = Const.FORM_MARGIN;
		formLayout.marginHeight = Const.FORM_MARGIN;

		this.shell.setLayout(formLayout);
		this.shell.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.Title"));

		int middle = this.props.getMiddlePct();
		int margin = Const.MARGIN;

		this.wOK = new Button(this.shell, SWT.PUSH);
		this.wOK.setText(BaseMessages.getString(PKG, "System.Button.OK"));
		FormData fd = new FormData();
		fd.right = new FormAttachment(50, -10);
		fd.bottom = new FormAttachment(100, 0);
		fd.width = 100;
		this.wOK.setLayoutData(fd);

		this.wCancel = new Button(this.shell, SWT.PUSH);
		this.wCancel.setText(BaseMessages.getString(PKG, "System.Button.Cancel"));
		fd = new FormData();
		fd.left = new FormAttachment(50, 10);
		fd.bottom = new FormAttachment(100, 0);
		fd.width = 100;
		this.wCancel.setLayoutData(fd);

		BaseStepDialog.positionBottomButtons(this.shell, new Button[] { this.wOK, this.wCancel }, margin, null);

		// Filename line
		this.wlName = new Label(this.shell, SWT.RIGHT);
		this.wlName.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.Name.Label"));
		this.props.setLook(this.wlName);
		this.fdlName = new FormData();
		this.fdlName.left = new FormAttachment(0, 0);
		this.fdlName.right = new FormAttachment(middle, -margin);
		this.fdlName.top = new FormAttachment(0, margin);
		this.wlName.setLayoutData(this.fdlName);
		this.wName = new Text(this.shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		this.props.setLook(this.wName);
		this.wName.addModifyListener(lsMod);
		this.fdName = new FormData();
		this.fdName.left = new FormAttachment(middle, 0);
		this.fdName.top = new FormAttachment(0, margin);
		this.fdName.right = new FormAttachment(100, 0);
		this.wName.setLayoutData(this.fdName);

		// Connection line
		this.wConnection = this.addConnectionLine(this.shell, this.wName, middle, margin);
		if (this.jobEntry.getDatabase() == null && this.jobMeta.nrDatabases() == 1) {
			this.wConnection.select(0);
		}
		this.wConnection.addModifyListener(lsMod);
		// Schema name line
		this.wlSchemaname = new Label(this.shell, SWT.RIGHT);
		this.wlSchemaname.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.Schemaname.Label"));
		this.props.setLook(this.wlSchemaname);
		this.fdlSchemaname = new FormData();
		this.fdlSchemaname.left = new FormAttachment(0, 0);
		this.fdlSchemaname.right = new FormAttachment(middle, 0);
		this.fdlSchemaname.top = new FormAttachment(this.wConnection, margin);
		this.wlSchemaname.setLayoutData(this.fdlSchemaname);

		this.wSchemaname = new TextVar(this.jobMeta, this.shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		this.props.setLook(this.wSchemaname);
		this.wSchemaname.setToolTipText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.Schemaname.Tooltip"));
		this.wSchemaname.addModifyListener(lsMod);
		this.fdSchemaname = new FormData();
		this.fdSchemaname.left = new FormAttachment(middle, 0);
		this.fdSchemaname.top = new FormAttachment(this.wConnection, margin);
		this.fdSchemaname.right = new FormAttachment(100, 0);
		this.wSchemaname.setLayoutData(this.fdSchemaname);

		// Table name line
		this.wlTablename = new Label(this.shell, SWT.RIGHT);
		this.wlTablename.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.Tablename.Label"));
		this.props.setLook(this.wlTablename);
		this.fdlTablename = new FormData();
		this.fdlTablename.left = new FormAttachment(0, 0);
		this.fdlTablename.right = new FormAttachment(middle, 0);
		this.fdlTablename.top = new FormAttachment(this.wSchemaname, margin);
		this.wlTablename.setLayoutData(this.fdlTablename);

		this.wbTable = new Button(this.shell, SWT.PUSH | SWT.CENTER);
		this.props.setLook(this.wbTable);
		this.wbTable.setText(BaseMessages.getString(PKG, "System.Button.Browse"));
		FormData fdbTable = new FormData();
		fdbTable.right = new FormAttachment(100, 0);
		fdbTable.top = new FormAttachment(this.wSchemaname, margin / 2);
		this.wbTable.setLayoutData(fdbTable);
		this.wbTable.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				JobEntryEvalTableContentDialog.this.getTableName();
			}
		});

		this.wTablename = new TextVar(this.jobMeta, this.shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		this.props.setLook(this.wTablename);
		this.wTablename.setToolTipText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.Tablename.Tooltip"));
		this.wTablename.addModifyListener(lsMod);
		this.fdTablename = new FormData();
		this.fdTablename.left = new FormAttachment(middle, 0);
		this.fdTablename.top = new FormAttachment(this.wSchemaname, margin);
		this.fdTablename.right = new FormAttachment(this.wbTable, -margin);
		this.wTablename.setLayoutData(this.fdTablename);

		// ////////////////////////
		// START OF Success GROUP///
		// ///////////////////////////////
		this.wSuccessGroup = new Group(this.shell, SWT.SHADOW_NONE);
		this.props.setLook(this.wSuccessGroup);
		this.wSuccessGroup.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.SuccessGroup.Group.Label"));

		FormLayout SuccessGroupLayout = new FormLayout();
		SuccessGroupLayout.marginWidth = 10;
		SuccessGroupLayout.marginHeight = 10;
		this.wSuccessGroup.setLayout(SuccessGroupLayout);

		//Success Condition
		this.wlSuccessCondition = new Label(this.wSuccessGroup, SWT.RIGHT);
		this.wlSuccessCondition.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.SuccessCondition.Label"));
		this.props.setLook(this.wlSuccessCondition);
		this.fdlSuccessCondition = new FormData();
		this.fdlSuccessCondition.left = new FormAttachment(0, -margin);
		this.fdlSuccessCondition.right = new FormAttachment(middle, -2 * margin);
		this.fdlSuccessCondition.top = new FormAttachment(0, margin);
		this.wlSuccessCondition.setLayoutData(this.fdlSuccessCondition);
		this.wSuccessCondition = new CCombo(this.wSuccessGroup, SWT.SINGLE | SWT.READ_ONLY | SWT.BORDER);
		this.wSuccessCondition.setItems(JobEntryEvalTableContent.successConditionsDesc);
		this.wSuccessCondition.select(0); // +1: starts at -1

		this.props.setLook(this.wSuccessCondition);
		this.fdSuccessCondition = new FormData();
		this.fdSuccessCondition.left = new FormAttachment(middle, -margin);
		this.fdSuccessCondition.top = new FormAttachment(0, margin);
		this.fdSuccessCondition.right = new FormAttachment(100, 0);
		this.wSuccessCondition.setLayoutData(this.fdSuccessCondition);
		this.wSuccessCondition.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				//activeSuccessCondition();

			}
		});

		// Success when number of errors less than
		this.wlLimit = new Label(this.wSuccessGroup, SWT.RIGHT);
		this.wlLimit.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.Limit.Label"));
		this.props.setLook(this.wlLimit);
		this.fdlLimit = new FormData();
		this.fdlLimit.left = new FormAttachment(0, -margin);
		this.fdlLimit.top = new FormAttachment(this.wSuccessCondition, margin);
		this.fdlLimit.right = new FormAttachment(middle, -2 * margin);
		this.wlLimit.setLayoutData(this.fdlLimit);

		this.wLimit = new TextVar(this.jobMeta, this.wSuccessGroup, SWT.SINGLE | SWT.LEFT | SWT.BORDER, BaseMessages.getString(PKG, "JobEntryEvalTableContent.Limit.Tooltip"));
		this.props.setLook(this.wLimit);
		this.wLimit.addModifyListener(lsMod);
		this.fdLimit = new FormData();
		this.fdLimit.left = new FormAttachment(middle, -margin);
		this.fdLimit.top = new FormAttachment(this.wSuccessCondition, margin);
		this.fdLimit.right = new FormAttachment(100, -margin);
		this.wLimit.setLayoutData(this.fdLimit);

		this.fdSuccessGroup = new FormData();
		this.fdSuccessGroup.left = new FormAttachment(0, margin);
		this.fdSuccessGroup.top = new FormAttachment(this.wbTable, margin);
		this.fdSuccessGroup.right = new FormAttachment(100, -margin);
		this.wSuccessGroup.setLayoutData(this.fdSuccessGroup);
		// ///////////////////////////////////////////////////////////
		// / END OF SuccessGroup GROUP
		// ///////////////////////////////////////////////////////////

		// ////////////////////////
		// START OF Custom GROUP///
		// ///////////////////////////////
		this.wCustomGroup = new Group(this.shell, SWT.SHADOW_NONE);
		this.props.setLook(this.wCustomGroup);
		this.wCustomGroup.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.CustomGroup.Group.Label"));

		FormLayout CustomGroupLayout = new FormLayout();
		CustomGroupLayout.marginWidth = 10;
		CustomGroupLayout.marginHeight = 10;
		this.wCustomGroup.setLayout(CustomGroupLayout);

		// custom SQL?
		this.wlcustomSQL = new Label(this.wCustomGroup, SWT.RIGHT);
		this.wlcustomSQL.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.customSQL.Label"));
		this.props.setLook(this.wlcustomSQL);
		this.fdlcustomSQL = new FormData();
		this.fdlcustomSQL.left = new FormAttachment(0, -margin);
		this.fdlcustomSQL.top = new FormAttachment(this.wSuccessGroup, margin);
		this.fdlcustomSQL.right = new FormAttachment(middle, -2 * margin);
		this.wlcustomSQL.setLayoutData(this.fdlcustomSQL);
		this.wcustomSQL = new Button(this.wCustomGroup, SWT.CHECK);
		this.props.setLook(this.wcustomSQL);
		this.wcustomSQL.setToolTipText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.customSQL.Tooltip"));
		this.fdcustomSQL = new FormData();
		this.fdcustomSQL.left = new FormAttachment(middle, -margin);
		this.fdcustomSQL.top = new FormAttachment(this.wSuccessGroup, margin);
		this.fdcustomSQL.right = new FormAttachment(100, 0);
		this.wcustomSQL.setLayoutData(this.fdcustomSQL);
		this.wcustomSQL.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{

				JobEntryEvalTableContentDialog.this.setCustomerSQL();
				JobEntryEvalTableContentDialog.this.jobEntry.setChanged();
			}
		});
		// use Variable substitution?
		this.wlUseSubs = new Label(this.wCustomGroup, SWT.RIGHT);
		this.wlUseSubs.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.UseVariableSubst.Label"));
		this.props.setLook(this.wlUseSubs);
		this.fdlUseSubs = new FormData();
		this.fdlUseSubs.left = new FormAttachment(0, -margin);
		this.fdlUseSubs.top = new FormAttachment(this.wcustomSQL, margin);
		this.fdlUseSubs.right = new FormAttachment(middle, -2 * margin);
		this.wlUseSubs.setLayoutData(this.fdlUseSubs);
		this.wUseSubs = new Button(this.wCustomGroup, SWT.CHECK);
		this.props.setLook(this.wUseSubs);
		this.wUseSubs.setToolTipText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.UseVariableSubst.Tooltip"));
		this.fdUseSubs = new FormData();
		this.fdUseSubs.left = new FormAttachment(middle, -margin);
		this.fdUseSubs.top = new FormAttachment(this.wcustomSQL, margin);
		this.fdUseSubs.right = new FormAttachment(100, 0);
		this.wUseSubs.setLayoutData(this.fdUseSubs);
		this.wUseSubs.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				JobEntryEvalTableContentDialog.this.jobEntry.setChanged();
			}
		});

		// clear result rows ?
		this.wlClearResultList = new Label(this.wCustomGroup, SWT.RIGHT);
		this.wlClearResultList.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.ClearResultList.Label"));
		this.props.setLook(this.wlClearResultList);
		this.fdlClearResultList = new FormData();
		this.fdlClearResultList.left = new FormAttachment(0, -margin);
		this.fdlClearResultList.top = new FormAttachment(this.wUseSubs, margin);
		this.fdlClearResultList.right = new FormAttachment(middle, -2 * margin);
		this.wlClearResultList.setLayoutData(this.fdlClearResultList);
		this.wClearResultList = new Button(this.wCustomGroup, SWT.CHECK);
		this.props.setLook(this.wClearResultList);
		this.wClearResultList.setToolTipText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.ClearResultList.Tooltip"));
		this.fdClearResultList = new FormData();
		this.fdClearResultList.left = new FormAttachment(middle, -margin);
		this.fdClearResultList.top = new FormAttachment(this.wUseSubs, margin);
		this.fdClearResultList.right = new FormAttachment(100, 0);
		this.wClearResultList.setLayoutData(this.fdClearResultList);
		this.wClearResultList.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				JobEntryEvalTableContentDialog.this.jobEntry.setChanged();
			}
		});

		// add rows to result?
		this.wlAddRowsToResult = new Label(this.wCustomGroup, SWT.RIGHT);
		this.wlAddRowsToResult.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.AddRowsToResult.Label"));
		this.props.setLook(this.wlAddRowsToResult);
		this.fdlAddRowsToResult = new FormData();
		this.fdlAddRowsToResult.left = new FormAttachment(0, -margin);
		this.fdlAddRowsToResult.top = new FormAttachment(this.wClearResultList, margin);
		this.fdlAddRowsToResult.right = new FormAttachment(middle, -2 * margin);
		this.wlAddRowsToResult.setLayoutData(this.fdlAddRowsToResult);
		this.wAddRowsToResult = new Button(this.wCustomGroup, SWT.CHECK);
		this.props.setLook(this.wAddRowsToResult);
		this.wAddRowsToResult.setToolTipText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.AddRowsToResult.Tooltip"));
		this.fdAddRowsToResult = new FormData();
		this.fdAddRowsToResult.left = new FormAttachment(middle, -margin);
		this.fdAddRowsToResult.top = new FormAttachment(this.wClearResultList, margin);
		this.fdAddRowsToResult.right = new FormAttachment(100, 0);
		this.wAddRowsToResult.setLayoutData(this.fdAddRowsToResult);
		this.wAddRowsToResult.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent e)
			{
				JobEntryEvalTableContentDialog.this.jobEntry.setChanged();
			}
		});

		this.wlPosition = new Label(this.wCustomGroup, SWT.NONE);
		this.props.setLook(this.wlPosition);
		this.fdlPosition = new FormData();
		this.fdlPosition.left = new FormAttachment(0, 0);
		this.fdlPosition.right = new FormAttachment(100, 0);
		//fdlPosition.top= new FormAttachment(wSQL , 0);
		this.fdlPosition.bottom = new FormAttachment(100, -margin);
		this.wlPosition.setLayoutData(this.fdlPosition);

		// Script line
		this.wlSQL = new Label(this.wCustomGroup, SWT.NONE);
		this.wlSQL.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.Script.Label"));
		this.props.setLook(this.wlSQL);
		this.fdlSQL = new FormData();
		this.fdlSQL.left = new FormAttachment(0, 0);
		this.fdlSQL.top = new FormAttachment(this.wAddRowsToResult, margin);
		this.wlSQL.setLayoutData(this.fdlSQL);

		this.wbSQLTable = new Button(this.wCustomGroup, SWT.PUSH | SWT.CENTER);
		this.props.setLook(this.wbSQLTable);
		this.wbSQLTable.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.GetSQLAndSelectStatement")); //$NON-NLS-1$
		FormData fdbSQLTable = new FormData();
		fdbSQLTable.right = new FormAttachment(100, 0);
		fdbSQLTable.top = new FormAttachment(this.wAddRowsToResult, margin);
		this.wbSQLTable.setLayoutData(fdbSQLTable);

		this.wSQL = new StyledTextComp(this.jobEntry, this.wCustomGroup, SWT.MULTI | SWT.LEFT | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL, "");
		this.props.setLook(this.wSQL, Props.WIDGET_STYLE_FIXED);
		this.wSQL.addModifyListener(lsMod);
		this.fdSQL = new FormData();
		this.fdSQL.left = new FormAttachment(0, 0);
		this.fdSQL.top = new FormAttachment(this.wbSQLTable, margin);
		this.fdSQL.right = new FormAttachment(100, -10);
		this.fdSQL.bottom = new FormAttachment(this.wlPosition, -margin);
		this.wSQL.setLayoutData(this.fdSQL);

		this.wSQL.addModifyListener(new ModifyListener()
		{
			@Override
			public void modifyText(ModifyEvent arg0)
			{
				JobEntryEvalTableContentDialog.this.setPosition();
			}

		});

		this.wSQL.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(KeyEvent e)
			{
				JobEntryEvalTableContentDialog.this.setPosition();
			}

			@Override
			public void keyReleased(KeyEvent e)
			{
				JobEntryEvalTableContentDialog.this.setPosition();
			}
		});
		this.wSQL.addFocusListener(new FocusAdapter()
		{
			@Override
			public void focusGained(FocusEvent e)
			{
				JobEntryEvalTableContentDialog.this.setPosition();
			}

			@Override
			public void focusLost(FocusEvent e)
			{
				JobEntryEvalTableContentDialog.this.setPosition();
			}
		});
		this.wSQL.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseDoubleClick(MouseEvent e)
			{
				JobEntryEvalTableContentDialog.this.setPosition();
			}

			@Override
			public void mouseDown(MouseEvent e)
			{
				JobEntryEvalTableContentDialog.this.setPosition();
			}

			@Override
			public void mouseUp(MouseEvent e)
			{
				JobEntryEvalTableContentDialog.this.setPosition();
			}
		});
		this.wSQL.addModifyListener(lsMod);

		// Text Higlighting
		this.wSQL.addLineStyleListener(new SQLValuesHighlight());

		this.fdCustomGroup = new FormData();
		this.fdCustomGroup.left = new FormAttachment(0, margin);
		this.fdCustomGroup.top = new FormAttachment(this.wSuccessGroup, margin);
		this.fdCustomGroup.right = new FormAttachment(100, -margin);
		this.fdCustomGroup.bottom = new FormAttachment(this.wOK, -margin);
		this.wCustomGroup.setLayoutData(this.fdCustomGroup);
		// ///////////////////////////////////////////////////////////
		// / END OF CustomGroup GROUP
		// ///////////////////////////////////////////////////////////

		// Add listeners
		this.lsCancel = new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				JobEntryEvalTableContentDialog.this.cancel();
			}
		};
		this.lsOK = new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				JobEntryEvalTableContentDialog.this.ok();
			}
		};
		this.lsbSQLTable = new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				JobEntryEvalTableContentDialog.this.getSQL();
			}
		};

		this.wCancel.addListener(SWT.Selection, this.lsCancel);
		this.wOK.addListener(SWT.Selection, this.lsOK);

		this.lsDef = new SelectionAdapter()
		{
			@Override
			public void widgetDefaultSelected(SelectionEvent e)
			{
				JobEntryEvalTableContentDialog.this.ok();
			}
		};

		this.wbSQLTable.addListener(SWT.Selection, this.lsbSQLTable);
		this.wName.addSelectionListener(this.lsDef);

		// Detect X or ALT-F4 or something that kills this window...
		this.shell.addShellListener(new ShellAdapter()
		{
			@Override
			public void shellClosed(ShellEvent e)
			{
				JobEntryEvalTableContentDialog.this.cancel();
			}
		});

		this.getData();
		this.setCustomerSQL();
		BaseStepDialog.setSize(this.shell);

		this.shell.open();
		this.props.setDialogSize(this.shell, "JobEntryEvalTableContentDialogSize");
		while (!this.shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return this.jobEntry;
	}

	private void getSQL()
	{
		DatabaseMeta inf = this.jobMeta.findDatabase(this.wConnection.getText());
		if (inf != null) {
			DatabaseExplorerDialog std = new DatabaseExplorerDialog(this.shell, SWT.NONE, inf, this.jobMeta.getDatabases());
			if (std.open()) {
				String sql = "SELECT *" + Const.CR + "FROM " + inf.getQuotedSchemaTableCombination(std.getSchemaName(), std.getTableName()) + Const.CR; //$NON-NLS-1$ //$NON-NLS-2$
				this.wSQL.setText(sql);

				MessageBox yn = new MessageBox(this.shell, SWT.YES | SWT.NO | SWT.CANCEL | SWT.ICON_QUESTION);
				yn.setMessage(BaseMessages.getString(PKG, "JobEntryEvalTableContent.IncludeFieldNamesInSQL")); //$NON-NLS-1$
				yn.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.DialogCaptionQuestion")); //$NON-NLS-1$
				int id = yn.open();
				switch (id) {
					case SWT.CANCEL:
						break;
					case SWT.NO:
						this.wSQL.setText(sql);
						break;
					case SWT.YES:
						Database db = new Database(loggingObject, inf);
						try {
							db.connect();
							RowMetaInterface fields = db.getQueryFields(sql, false);
							if (fields != null) {
								sql = "SELECT" + Const.CR; //$NON-NLS-1$
								for (int i = 0; i < fields.size(); i++) {
									ValueMetaInterface field = fields.getValueMeta(i);
									if (i == 0) {
										sql += "  ";
									}
									else {
										sql += ", "; //$NON-NLS-1$ //$NON-NLS-2$
									}
									sql += inf.quoteField(field.getName()) + Const.CR;
								}
								sql += "FROM " + inf.getQuotedSchemaTableCombination(std.getSchemaName(), std.getTableName()) + Const.CR; //$NON-NLS-1$
								this.wSQL.setText(sql);
							}
							else {
								MessageBox mb = new MessageBox(this.shell, SWT.OK | SWT.ICON_ERROR);
								mb.setMessage(BaseMessages.getString(PKG, "JobEntryEvalTableContent.ERROR_CouldNotRetrieveFields") + Const.CR + BaseMessages.getString(PKG, "JobEntryEvalTableContent.PerhapsNoPermissions")); //$NON-NLS-1$ //$NON-NLS-2$
								mb.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.DialogCaptionError2")); //$NON-NLS-1$
								mb.open();
							}
						}
						catch (KettleException e) {
							MessageBox mb = new MessageBox(this.shell, SWT.OK | SWT.ICON_ERROR);
							mb.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.DialogCaptionError3")); //$NON-NLS-1$
							mb.setMessage(BaseMessages.getString(PKG, "JobEntryEvalTableContent.AnErrorOccurred") + Const.CR + e.getMessage()); //$NON-NLS-1$
							mb.open();
						}
						finally {
							db.disconnect();
						}
						break;
				}
			}
		}
		else {
			MessageBox mb = new MessageBox(this.shell, SWT.OK | SWT.ICON_ERROR);
			mb.setMessage(BaseMessages.getString(PKG, "JobEntryEvalTableContent.ConnectionNoLongerAvailable")); //$NON-NLS-1$
			mb.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.DialogCaptionError4")); //$NON-NLS-1$
			mb.open();
		}

	}

	public void setPosition()
	{

		String scr = this.wSQL.getText();
		int linenr = this.wSQL.getLineAtOffset(this.wSQL.getCaretOffset()) + 1;
		int posnr = this.wSQL.getCaretOffset();

		// Go back from position to last CR: how many positions?
		int colnr = 0;
		while (posnr > 0 && scr.charAt(posnr - 1) != '\n' && scr.charAt(posnr - 1) != '\r') {
			posnr--;
			colnr++;
		}
		this.wlPosition.setText(BaseMessages.getString(PKG, "JobEntryEvalTableContent.Position.Label", "" + linenr, "" + colnr));

	}

	private void setCustomerSQL()
	{
		this.wlSQL.setEnabled(this.wcustomSQL.getSelection());
		this.wSQL.setEnabled(this.wcustomSQL.getSelection());
		this.wlAddRowsToResult.setEnabled(this.wcustomSQL.getSelection());
		this.wAddRowsToResult.setEnabled(this.wcustomSQL.getSelection());
		this.wlClearResultList.setEnabled(this.wcustomSQL.getSelection());
		this.wClearResultList.setEnabled(this.wcustomSQL.getSelection());
		this.wlUseSubs.setEnabled(this.wcustomSQL.getSelection());
		this.wbSQLTable.setEnabled(this.wcustomSQL.getSelection());
		this.wUseSubs.setEnabled(this.wcustomSQL.getSelection());
		this.wbTable.setEnabled(!this.wcustomSQL.getSelection());
		this.wTablename.setEnabled(!this.wcustomSQL.getSelection());
		this.wlTablename.setEnabled(!this.wcustomSQL.getSelection());
		this.wlSchemaname.setEnabled(!this.wcustomSQL.getSelection());
		this.wSchemaname.setEnabled(!this.wcustomSQL.getSelection());
	}

	public void dispose()
	{
		WindowProperty winprop = new WindowProperty(this.shell);
		this.props.setScreen(winprop);
		this.shell.dispose();
	}

	/**
	 * Copy information from the meta-data input to the dialog fields.
	 */
	public void getData()
	{
		if (this.jobEntry.getName() != null) {
			this.wName.setText(this.jobEntry.getName());
		}

		if (this.jobEntry.getDatabase() != null) {
			this.wConnection.setText(this.jobEntry.getDatabase().getName());
		}

		if (this.jobEntry.schemaname != null) {
			this.wSchemaname.setText(this.jobEntry.schemaname);
		}
		if (this.jobEntry.tablename != null) {
			this.wTablename.setText(this.jobEntry.tablename);
		}

		this.wSuccessCondition.setText(JobEntryEvalTableContent.getSuccessConditionDesc(this.jobEntry.successCondition));
		if (this.jobEntry.limit != null) {
			this.wLimit.setText(this.jobEntry.limit);
		}
		else {
			this.wLimit.setText("0");
		}

		this.wcustomSQL.setSelection(this.jobEntry.iscustomSQL);
		this.wUseSubs.setSelection(this.jobEntry.isUseVars);
		this.wClearResultList.setSelection(this.jobEntry.isClearResultList);
		this.wAddRowsToResult.setSelection(this.jobEntry.isAddRowsResult);
		if (this.jobEntry.customSQL != null) {
			this.wSQL.setText(this.jobEntry.customSQL);
		}

		this.wName.selectAll();
	}

	private void cancel()
	{
		this.jobEntry.setChanged(this.changed);
		this.jobEntry = null;
		this.dispose();
	}

	private void ok()
	{
		if (Const.isEmpty(this.wName.getText())) {
			MessageBox mb = new MessageBox(this.shell, SWT.OK | SWT.ICON_ERROR);
			mb.setMessage("Please give this job entry a name.");
			mb.setText("Enter the name of the job entry");
			mb.open();
			return;
		}
		this.jobEntry.setName(this.wName.getText());
		this.jobEntry.setDatabase(this.jobMeta.findDatabase(this.wConnection.getText()));

		this.jobEntry.schemaname = this.wSchemaname.getText();
		this.jobEntry.tablename = this.wTablename.getText();
		this.jobEntry.successCondition = JobEntryEvalTableContent.getSuccessConditionByDesc(this.wSuccessCondition.getText());
		this.jobEntry.limit = this.wLimit.getText();
		this.jobEntry.iscustomSQL = this.wcustomSQL.getSelection();
		this.jobEntry.isUseVars = this.wUseSubs.getSelection();
		this.jobEntry.isAddRowsResult = this.wAddRowsToResult.getSelection();
		this.jobEntry.isClearResultList = this.wClearResultList.getSelection();

		this.jobEntry.customSQL = this.wSQL.getText();
		this.dispose();
	}

	private void getTableName()
	{
		// New class: SelectTableDialog
		int connr = this.wConnection.getSelectionIndex();
		if (connr >= 0) {
			DatabaseMeta inf = this.jobMeta.getDatabase(connr);

			DatabaseExplorerDialog std = new DatabaseExplorerDialog(this.shell, SWT.NONE, inf, this.jobMeta.getDatabases());
			std.setSelectedSchemaAndTable(this.wSchemaname.getText(), this.wTablename.getText());
			if (std.open()) {
				this.wTablename.setText(Const.NVL(std.getTableName(), ""));
			}
		}
		else {
			MessageBox mb = new MessageBox(this.shell, SWT.OK | SWT.ICON_ERROR);
			mb.setMessage(BaseMessages.getString(PKG, "JobEntryEvalTableContent.ConnectionError2.DialogMessage"));
			mb.setText(BaseMessages.getString(PKG, "System.Dialog.Error.Title"));
			mb.open();
		}
	}

}
