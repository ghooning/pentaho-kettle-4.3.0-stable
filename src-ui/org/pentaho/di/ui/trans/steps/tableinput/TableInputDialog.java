/*******************************************************************************
 *
 * Pentaho Data Integration
 *
 * Copyright (C) 2002-2012 by Pentaho : http://www.pentaho.com
 *
 *******************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 ******************************************************************************/

package org.pentaho.di.ui.trans.steps.tableinput;

import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.events.FocusAdapter;
import org.eclipse.swt.events.FocusEvent;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.ShellAdapter;
import org.eclipse.swt.events.ShellEvent;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.Props;
import org.pentaho.di.core.database.Database;
import org.pentaho.di.core.database.DatabaseMeta;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.row.RowMetaInterface;
import org.pentaho.di.core.row.ValueMetaInterface;
import org.pentaho.di.i18n.BaseMessages;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;
import org.pentaho.di.trans.TransPreviewFactory;
import org.pentaho.di.trans.step.BaseStepMeta;
import org.pentaho.di.trans.step.StepDialogInterface;
import org.pentaho.di.trans.step.StepMeta;
import org.pentaho.di.trans.step.errorhandling.StreamInterface;
import org.pentaho.di.trans.steps.tableinput.TableInputMeta;
import org.pentaho.di.ui.core.database.dialog.DatabaseExplorerDialog;
import org.pentaho.di.ui.core.dialog.EnterNumberDialog;
import org.pentaho.di.ui.core.dialog.EnterTextDialog;
import org.pentaho.di.ui.core.dialog.PreviewRowsDialog;
import org.pentaho.di.ui.core.widget.StyledTextComp;
import org.pentaho.di.ui.core.widget.TextVar;
import org.pentaho.di.ui.spoon.job.JobGraph;
import org.pentaho.di.ui.trans.dialog.TransPreviewProgressDialog;
import org.pentaho.di.ui.trans.step.BaseStepDialog;

public class TableInputDialog extends BaseStepDialog implements StepDialogInterface
{
	private static Class<?> PKG = TableInputMeta.class; // for i18n purposes, needed by Translator2!!   $NON-NLS-1$

	private CCombo wConnection;

	private Label wlSQL;

	private StyledTextComp wSQL;

	private FormData fdlSQL, fdSQL;

	private Label wlDatefrom;

	private CCombo wDatefrom;

	private FormData fdlDatefrom, fdDatefrom;

	private Listener lsDateform;

	private Label wlLimit;

	private TextVar wLimit;

	private FormData fdlLimit, fdLimit;

	private Label wlEachRow;

	private Button wEachRow;

	private FormData fdlEachRow, fdEachRow;

	private Label wlVariables;

	private Button wVariables;

	private FormData fdlVariables, fdVariables;

	private Label wlLazyConversion;

	private Button wLazyConversion;

	private FormData fdlLazyConversion, fdLazyConversion;

	private Button wbTable;

	private FormData fdbTable;

	private Listener lsbTable;

	private final TableInputMeta input;

	private boolean changedInDialog;

	private Label wlPosition;

	private FormData fdlPosition;

	public TableInputDialog(Shell parent, Object in, TransMeta transMeta, String sname)
	{
		super(parent, (BaseStepMeta) in, transMeta, sname);
		this.input = (TableInputMeta) in;
	}

	@Override
	public String open()
	{
		Shell parent = this.getParent();
		Display display = parent.getDisplay();

		this.shell = new Shell(parent, SWT.DIALOG_TRIM | SWT.RESIZE | SWT.MAX | SWT.MIN);
		this.props.setLook(this.shell);
		this.setShellImage(this.shell, this.input);

		ModifyListener lsMod = new ModifyListener()
		{
			@Override
			public void modifyText(ModifyEvent e)
			{
				TableInputDialog.this.changedInDialog = false; // for prompting if dialog is simply closed
				TableInputDialog.this.input.setChanged();
			}
		};
		this.changed = this.input.hasChanged();

		FormLayout formLayout = new FormLayout();
		formLayout.marginWidth = Const.FORM_MARGIN;
		formLayout.marginHeight = Const.FORM_MARGIN;

		this.shell.setLayout(formLayout);
		this.shell.setText(BaseMessages.getString(PKG, "TableInputDialog.TableInput")); //$NON-NLS-1$

		int middle = this.props.getMiddlePct();
		int margin = Const.MARGIN;

		// Stepname line
		this.wlStepname = new Label(this.shell, SWT.RIGHT);
		this.wlStepname.setText(BaseMessages.getString(PKG, "TableInputDialog.StepName")); //$NON-NLS-1$
		this.props.setLook(this.wlStepname);
		this.fdlStepname = new FormData();
		this.fdlStepname.left = new FormAttachment(0, 0);
		this.fdlStepname.right = new FormAttachment(middle, -margin);
		this.fdlStepname.top = new FormAttachment(0, margin);
		this.wlStepname.setLayoutData(this.fdlStepname);
		this.wStepname = new Text(this.shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		this.wStepname.setText(this.stepname);
		this.props.setLook(this.wStepname);
		this.wStepname.addModifyListener(lsMod);
		this.fdStepname = new FormData();
		this.fdStepname.left = new FormAttachment(middle, 0);
		this.fdStepname.top = new FormAttachment(0, margin);
		this.fdStepname.right = new FormAttachment(100, 0);
		this.wStepname.setLayoutData(this.fdStepname);

		// Connection line
		this.wConnection = this.addConnectionLine(this.shell, this.wStepname, middle, margin);
		if (this.input.getDatabaseMeta() == null && this.transMeta.nrDatabases() == 1) {
			this.wConnection.select(0);
		}
		this.wConnection.addModifyListener(lsMod);

		// Some buttons
		this.wOK = new Button(this.shell, SWT.PUSH);
		this.wOK.setText(BaseMessages.getString(PKG, "System.Button.OK")); //$NON-NLS-1$
		this.wPreview = new Button(this.shell, SWT.PUSH);
		this.wPreview.setText(BaseMessages.getString(PKG, "System.Button.Preview")); //$NON-NLS-1$
		this.wCancel = new Button(this.shell, SWT.PUSH);
		this.wCancel.setText(BaseMessages.getString(PKG, "System.Button.Cancel")); //$NON-NLS-1$

		this.setButtonPositions(new Button[] { this.wOK, this.wPreview, this.wCancel }, margin, null);

		// Limit input ...
		this.wlLimit = new Label(this.shell, SWT.RIGHT);
		this.wlLimit.setText(BaseMessages.getString(PKG, "TableInputDialog.LimitSize")); //$NON-NLS-1$
		this.props.setLook(this.wlLimit);
		this.fdlLimit = new FormData();
		this.fdlLimit.left = new FormAttachment(0, 0);
		this.fdlLimit.right = new FormAttachment(middle, -margin);
		this.fdlLimit.bottom = new FormAttachment(this.wOK, -2 * margin);
		this.wlLimit.setLayoutData(this.fdlLimit);
		this.wLimit = new TextVar(this.transMeta, this.shell, SWT.SINGLE | SWT.LEFT | SWT.BORDER);
		this.props.setLook(this.wLimit);
		this.wLimit.addModifyListener(lsMod);
		this.fdLimit = new FormData();
		this.fdLimit.left = new FormAttachment(middle, 0);
		this.fdLimit.right = new FormAttachment(100, 0);
		this.fdLimit.bottom = new FormAttachment(this.wOK, -2 * margin);
		this.wLimit.setLayoutData(this.fdLimit);

		// Execute for each row?
		this.wlEachRow = new Label(this.shell, SWT.RIGHT);
		this.wlEachRow.setText(BaseMessages.getString(PKG, "TableInputDialog.ExecuteForEachRow")); //$NON-NLS-1$
		this.props.setLook(this.wlEachRow);
		this.fdlEachRow = new FormData();
		this.fdlEachRow.left = new FormAttachment(0, 0);
		this.fdlEachRow.right = new FormAttachment(middle, -margin);
		this.fdlEachRow.bottom = new FormAttachment(this.wLimit, -margin);
		this.wlEachRow.setLayoutData(this.fdlEachRow);
		this.wEachRow = new Button(this.shell, SWT.CHECK);
		this.props.setLook(this.wEachRow);
		this.fdEachRow = new FormData();
		this.fdEachRow.left = new FormAttachment(middle, 0);
		this.fdEachRow.right = new FormAttachment(100, 0);
		this.fdEachRow.bottom = new FormAttachment(this.wLimit, -margin);
		this.wEachRow.setLayoutData(this.fdEachRow);
		SelectionAdapter lsSelMod = new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				TableInputDialog.this.input.setChanged();
			}
		};
		this.wEachRow.addSelectionListener(lsSelMod);

		// Read date from...
		this.wlDatefrom = new Label(this.shell, SWT.RIGHT);
		this.wlDatefrom.setText(BaseMessages.getString(PKG, "TableInputDialog.InsertDataFromStep")); //$NON-NLS-1$
		this.props.setLook(this.wlDatefrom);
		this.fdlDatefrom = new FormData();
		this.fdlDatefrom.left = new FormAttachment(0, 0);
		this.fdlDatefrom.right = new FormAttachment(middle, -margin);
		this.fdlDatefrom.bottom = new FormAttachment(this.wEachRow, -margin);
		this.wlDatefrom.setLayoutData(this.fdlDatefrom);
		this.wDatefrom = new CCombo(this.shell, SWT.BORDER);
		this.props.setLook(this.wDatefrom);

		List<StepMeta> previousSteps = this.transMeta.findPreviousSteps(this.transMeta.findStep(this.stepname));
		for (StepMeta stepMeta : previousSteps) {
			this.wDatefrom.add(stepMeta.getName());
		}

		this.wDatefrom.addModifyListener(lsMod);
		this.fdDatefrom = new FormData();
		this.fdDatefrom.left = new FormAttachment(middle, 0);
		this.fdDatefrom.right = new FormAttachment(100, 0);
		this.fdDatefrom.bottom = new FormAttachment(this.wEachRow, -margin);
		this.wDatefrom.setLayoutData(this.fdDatefrom);

		// Replace variables in SQL?
		//
		this.wlVariables = new Label(this.shell, SWT.RIGHT);
		this.wlVariables.setText(BaseMessages.getString(PKG, "TableInputDialog.ReplaceVariables")); //$NON-NLS-1$
		this.props.setLook(this.wlVariables);
		this.fdlVariables = new FormData();
		this.fdlVariables.left = new FormAttachment(0, 0);
		this.fdlVariables.right = new FormAttachment(middle, -margin);
		this.fdlVariables.bottom = new FormAttachment(this.wDatefrom, -margin);
		this.wlVariables.setLayoutData(this.fdlVariables);
		this.wVariables = new Button(this.shell, SWT.CHECK);
		this.props.setLook(this.wVariables);
		this.fdVariables = new FormData();
		this.fdVariables.left = new FormAttachment(middle, 0);
		this.fdVariables.right = new FormAttachment(100, 0);
		this.fdVariables.bottom = new FormAttachment(this.wDatefrom, -margin);
		this.wVariables.setLayoutData(this.fdVariables);
		this.wVariables.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				TableInputDialog.this.input.setChanged();
				TableInputDialog.this.setSQLToolTip();
			}
		});

		// Lazy conversion?
		//
		this.wlLazyConversion = new Label(this.shell, SWT.RIGHT);
		this.wlLazyConversion.setText(BaseMessages.getString(PKG, "TableInputDialog.LazyConversion")); //$NON-NLS-1$
		this.props.setLook(this.wlLazyConversion);
		this.fdlLazyConversion = new FormData();
		this.fdlLazyConversion.left = new FormAttachment(0, 0);
		this.fdlLazyConversion.right = new FormAttachment(middle, -margin);
		this.fdlLazyConversion.bottom = new FormAttachment(this.wVariables, -margin);
		this.wlLazyConversion.setLayoutData(this.fdlLazyConversion);
		this.wLazyConversion = new Button(this.shell, SWT.CHECK);
		this.props.setLook(this.wLazyConversion);
		this.fdLazyConversion = new FormData();
		this.fdLazyConversion.left = new FormAttachment(middle, 0);
		this.fdLazyConversion.right = new FormAttachment(100, 0);
		this.fdLazyConversion.bottom = new FormAttachment(this.wVariables, -margin);
		this.wLazyConversion.setLayoutData(this.fdLazyConversion);
		this.wLazyConversion.addSelectionListener(new SelectionAdapter()
		{
			@Override
			public void widgetSelected(SelectionEvent arg0)
			{
				TableInputDialog.this.input.setChanged();
				TableInputDialog.this.setSQLToolTip();
			}
		});

		this.wlPosition = new Label(this.shell, SWT.NONE);
		this.props.setLook(this.wlPosition);
		this.fdlPosition = new FormData();
		this.fdlPosition.left = new FormAttachment(0, 0);
		this.fdlPosition.right = new FormAttachment(100, 0);
		this.fdlPosition.bottom = new FormAttachment(this.wLazyConversion, -margin);
		this.wlPosition.setLayoutData(this.fdlPosition);

		// Table line...
		this.wlSQL = new Label(this.shell, SWT.NONE);
		this.wlSQL.setText(BaseMessages.getString(PKG, "TableInputDialog.SQL")); //$NON-NLS-1$
		this.props.setLook(this.wlSQL);
		this.fdlSQL = new FormData();
		this.fdlSQL.left = new FormAttachment(0, 0);
		this.fdlSQL.top = new FormAttachment(this.wConnection, margin * 2);
		this.wlSQL.setLayoutData(this.fdlSQL);

		this.wbTable = new Button(this.shell, SWT.PUSH | SWT.CENTER);
		this.props.setLook(this.wbTable);
		this.wbTable.setText(BaseMessages.getString(PKG, "TableInputDialog.GetSQLAndSelectStatement")); //$NON-NLS-1$
		this.fdbTable = new FormData();
		this.fdbTable.right = new FormAttachment(100, 0);
		this.fdbTable.top = new FormAttachment(this.wConnection, margin * 2);
		this.wbTable.setLayoutData(this.fdbTable);

		this.wSQL = new StyledTextComp(this.transMeta, this.shell, SWT.MULTI | SWT.LEFT | SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL, "");
		this.props.setLook(this.wSQL, Props.WIDGET_STYLE_FIXED);
		this.wSQL.addModifyListener(lsMod);
		this.fdSQL = new FormData();
		this.fdSQL.left = new FormAttachment(0, 0);
		this.fdSQL.top = new FormAttachment(this.wbTable, margin);
		this.fdSQL.right = new FormAttachment(100, -2 * margin);
		this.fdSQL.bottom = new FormAttachment(this.wlPosition, -margin);
		this.wSQL.setLayoutData(this.fdSQL);
		this.wSQL.addModifyListener(new ModifyListener()
		{
			@Override
			public void modifyText(ModifyEvent arg0)
			{
				TableInputDialog.this.setSQLToolTip();
				TableInputDialog.this.setPosition();
			}
		});

		this.wSQL.addKeyListener(new KeyAdapter()
		{
			@Override
			public void keyPressed(KeyEvent e)
			{
				TableInputDialog.this.setPosition();
			}

			@Override
			public void keyReleased(KeyEvent e)
			{
				TableInputDialog.this.setPosition();
			}
		});
		this.wSQL.addFocusListener(new FocusAdapter()
		{
			@Override
			public void focusGained(FocusEvent e)
			{
				TableInputDialog.this.setPosition();
			}

			@Override
			public void focusLost(FocusEvent e)
			{
				TableInputDialog.this.setPosition();
			}
		});
		this.wSQL.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseDoubleClick(MouseEvent e)
			{
				TableInputDialog.this.setPosition();
			}

			@Override
			public void mouseDown(MouseEvent e)
			{
				TableInputDialog.this.setPosition();
			}

			@Override
			public void mouseUp(MouseEvent e)
			{
				TableInputDialog.this.setPosition();
			}
		});

		// Text Higlighting
		this.wSQL.addLineStyleListener(new SQLValuesHighlight());

		// Add listeners
		this.lsCancel = new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				TableInputDialog.this.cancel();
			}
		};
		this.lsPreview = new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				TableInputDialog.this.preview();
			}
		};
		this.lsOK = new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				TableInputDialog.this.ok();
			}
		};
		this.lsbTable = new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				TableInputDialog.this.getSQL();
			}
		};
		this.lsDateform = new Listener()
		{
			@Override
			public void handleEvent(Event e)
			{
				TableInputDialog.this.setFags();
			}
		};

		this.wCancel.addListener(SWT.Selection, this.lsCancel);
		this.wPreview.addListener(SWT.Selection, this.lsPreview);
		this.wOK.addListener(SWT.Selection, this.lsOK);
		this.wbTable.addListener(SWT.Selection, this.lsbTable);
		this.wDatefrom.addListener(SWT.Selection, this.lsDateform);
		this.wDatefrom.addListener(SWT.FocusOut, this.lsDateform);

		this.lsDef = new SelectionAdapter()
		{
			@Override
			public void widgetDefaultSelected(SelectionEvent e)
			{
				TableInputDialog.this.ok();
			}
		};

		this.wStepname.addSelectionListener(this.lsDef);
		this.wLimit.addSelectionListener(this.lsDef);

		// Detect X or ALT-F4 or something that kills this window...
		this.shell.addShellListener(new ShellAdapter()
		{
			@Override
			public void shellClosed(ShellEvent e)
			{
				TableInputDialog.this.checkCancel(e);
			}
		});

		this.getData();
		this.changedInDialog = false; // for prompting if dialog is simply closed
		this.input.setChanged(this.changed);

		// Set the shell size, based upon previous time...
		this.setSize();

		this.shell.open();
		while (!this.shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		return this.stepname;
	}

	public void setPosition()
	{

		String scr = this.wSQL.getText();
		int linenr = this.wSQL.getLineAtOffset(this.wSQL.getCaretOffset()) + 1;
		int posnr = this.wSQL.getCaretOffset();

		// Go back from position to last CR: how many positions?
		int colnr = 0;
		while (posnr > 0 && scr.charAt(posnr - 1) != '\n' && scr.charAt(posnr - 1) != '\r') {
			posnr--;
			colnr++;
		}
		this.wlPosition.setText(BaseMessages.getString(PKG, "TableInputDialog.Position.Label", "" + linenr, "" + colnr));

	}

	protected void setSQLToolTip()
	{
		if (this.wVariables.getSelection()) {
			this.wSQL.setToolTipText(this.transMeta.environmentSubstitute(this.wSQL.getText()));
		}
	}

	/**
	 * Copy information from the meta-data input to the dialog fields.
	 */
	public void getData()
	{
		if (this.input.getSQL() != null) {
			this.wSQL.setText(this.input.getSQL());
		}
		if (this.input.getDatabaseMeta() != null) {
			this.wConnection.setText(this.input.getDatabaseMeta().getName());
		}
		this.wLimit.setText(Const.NVL(this.input.getRowLimit(), "")); //$NON-NLS-1$

		StreamInterface infoStream = this.input.getStepIOMeta().getInfoStreams().get(0);
		if (infoStream.getStepMeta() != null) {
			this.wDatefrom.setText(infoStream.getStepname());
			this.wEachRow.setSelection(this.input.isExecuteEachInputRow());
		}
		else {
			this.wEachRow.setEnabled(false);
			this.wlEachRow.setEnabled(false);
		}

		this.wVariables.setSelection(this.input.isVariableReplacementActive());
		this.wLazyConversion.setSelection(this.input.isLazyConversionActive());

		this.wStepname.selectAll();
		this.setSQLToolTip();
	}

	private void checkCancel(ShellEvent e)
	{
		if (this.changedInDialog) {
			int save = JobGraph.showChangedWarning(this.shell, this.wStepname.getText());
			if (save == SWT.CANCEL) {
				e.doit = false;
			}
			else if (save == SWT.YES) {
				this.ok();
			}
			else {
				this.cancel();
			}
		}
		else {
			this.cancel();
		}
	}

	private void cancel()
	{
		this.stepname = null;
		this.input.setChanged(this.changed);
		this.dispose();
	}

	private void getInfo(TableInputMeta meta, boolean preview)
	{
		meta.setSQL(preview && !Const.isEmpty(this.wSQL.getSelectionText()) ? this.wSQL.getSelectionText() : this.wSQL.getText());
		meta.setDatabaseMeta(this.transMeta.findDatabase(this.wConnection.getText()));
		meta.setRowLimit(this.wLimit.getText());
		StreamInterface infoStream = this.input.getStepIOMeta().getInfoStreams().get(0);
		infoStream.setStepMeta(this.transMeta.findStep(this.wDatefrom.getText()));
		meta.setExecuteEachInputRow(this.wEachRow.getSelection());
		meta.setVariableReplacementActive(this.wVariables.getSelection());
		meta.setLazyConversionActive(this.wLazyConversion.getSelection());
	}

	private void ok()
	{
		if (Const.isEmpty(this.wStepname.getText())) {
			return;
		}

		this.stepname = this.wStepname.getText(); // return value
		// copy info to TextFileInputMeta class (input)

		this.getInfo(this.input, false);

		if (this.input.getDatabaseMeta() == null) {
			MessageBox mb = new MessageBox(this.shell, SWT.OK | SWT.ICON_ERROR);
			mb.setMessage(BaseMessages.getString(PKG, "TableInputDialog.SelectValidConnection")); //$NON-NLS-1$
			mb.setText(BaseMessages.getString(PKG, "TableInputDialog.DialogCaptionError")); //$NON-NLS-1$
			mb.open();
			return;
		}

		this.dispose();
	}

	private void getSQL()
	{
		DatabaseMeta inf = this.transMeta.findDatabase(this.wConnection.getText());
		if (inf != null) {
			DatabaseExplorerDialog std = new DatabaseExplorerDialog(this.shell, SWT.NONE, inf, this.transMeta.getDatabases());
			if (std.open()) {
				String sql = "SELECT *" + Const.CR + "FROM " + inf.getQuotedSchemaTableCombination(std.getSchemaName(), std.getTableName()) + Const.CR; //$NON-NLS-1$ //$NON-NLS-2$
				this.wSQL.setText(sql);

				MessageBox yn = new MessageBox(this.shell, SWT.YES | SWT.NO | SWT.CANCEL | SWT.ICON_QUESTION);
				yn.setMessage(BaseMessages.getString(PKG, "TableInputDialog.IncludeFieldNamesInSQL")); //$NON-NLS-1$
				yn.setText(BaseMessages.getString(PKG, "TableInputDialog.DialogCaptionQuestion")); //$NON-NLS-1$
				int id = yn.open();
				switch (id) {
					case SWT.CANCEL:
						break;
					case SWT.NO:
						this.wSQL.setText(sql);
						break;
					case SWT.YES:
						Database db = new Database(loggingObject, inf);
						db.shareVariablesWith(this.transMeta);
						try {
							db.connect();
							RowMetaInterface fields = db.getQueryFields(sql, false);
							if (fields != null) {
								sql = "SELECT" + Const.CR; //$NON-NLS-1$
								for (int i = 0; i < fields.size(); i++) {
									ValueMetaInterface field = fields.getValueMeta(i);
									if (i == 0) {
										sql += "  ";
									}
									else {
										sql += ", "; //$NON-NLS-1$ //$NON-NLS-2$
									}
									sql += inf.quoteField(field.getName()) + Const.CR;
								}
								sql += "FROM " + inf.getQuotedSchemaTableCombination(std.getSchemaName(), std.getTableName()) + Const.CR; //$NON-NLS-1$
								this.wSQL.setText(sql);
							}
							else {
								MessageBox mb = new MessageBox(this.shell, SWT.OK | SWT.ICON_ERROR);
								mb.setMessage(BaseMessages.getString(PKG, "TableInputDialog.ERROR_CouldNotRetrieveFields") + Const.CR + BaseMessages.getString(PKG, "TableInputDialog.PerhapsNoPermissions")); //$NON-NLS-1$ //$NON-NLS-2$
								mb.setText(BaseMessages.getString(PKG, "TableInputDialog.DialogCaptionError2")); //$NON-NLS-1$
								mb.open();
							}
						}
						catch (KettleException e) {
							MessageBox mb = new MessageBox(this.shell, SWT.OK | SWT.ICON_ERROR);
							mb.setText(BaseMessages.getString(PKG, "TableInputDialog.DialogCaptionError3")); //$NON-NLS-1$
							mb.setMessage(BaseMessages.getString(PKG, "TableInputDialog.AnErrorOccurred") + Const.CR + e.getMessage()); //$NON-NLS-1$
							mb.open();
						}
						finally {
							db.disconnect();
						}
						break;
				}
			}
		}
		else {
			MessageBox mb = new MessageBox(this.shell, SWT.OK | SWT.ICON_ERROR);
			mb.setMessage(BaseMessages.getString(PKG, "TableInputDialog.ConnectionNoLongerAvailable")); //$NON-NLS-1$
			mb.setText(BaseMessages.getString(PKG, "TableInputDialog.DialogCaptionError4")); //$NON-NLS-1$
			mb.open();
		}

	}

	private void setFags()
	{
		if (this.wDatefrom.getText() != null && this.wDatefrom.getText().length() > 0) {
			// The foreach check box... 
			this.wEachRow.setEnabled(true);
			this.wlEachRow.setEnabled(true);

			// The preview button...
			this.wPreview.setEnabled(false);
		}
		else {
			// The foreach check box... 
			this.wEachRow.setEnabled(false);
			this.wEachRow.setSelection(false);
			this.wlEachRow.setEnabled(false);

			// The preview button...
			this.wPreview.setEnabled(true);
		}

	}

	/**
	 * Preview the data generated by this step.
	 * This generates a transformation using this step & a dummy and previews it.
	 *
	 */
	private void preview()
	{
		// Create the table input reader step...
		TableInputMeta oneMeta = new TableInputMeta();
		this.getInfo(oneMeta, true);

		TransMeta previewMeta = TransPreviewFactory.generatePreviewTransformation(this.transMeta, oneMeta, this.wStepname.getText());

		EnterNumberDialog numberDialog = new EnterNumberDialog(this.shell, this.props.getDefaultPreviewSize(), BaseMessages.getString(PKG, "TableInputDialog.EnterPreviewSize"), BaseMessages.getString(PKG, "TableInputDialog.NumberOfRowsToPreview")); //$NON-NLS-1$ //$NON-NLS-2$
		int previewSize = numberDialog.open();
		if (previewSize > 0) {
			TransPreviewProgressDialog progressDialog = new TransPreviewProgressDialog(this.shell, previewMeta, new String[] { this.wStepname.getText() }, new int[] { previewSize });
			progressDialog.open();

			Trans trans = progressDialog.getTrans();
			String loggingText = progressDialog.getLoggingText();

			if (!progressDialog.isCancelled()) {
				if (trans.getResult() != null && trans.getResult().getNrErrors() > 0) {
					EnterTextDialog etd = new EnterTextDialog(this.shell, BaseMessages.getString(PKG, "System.Dialog.PreviewError.Title"), BaseMessages.getString(PKG, "System.Dialog.PreviewError.Message"), loggingText, true);
					etd.setReadOnly();
					etd.open();
				}
				else {
					PreviewRowsDialog prd = new PreviewRowsDialog(this.shell, this.transMeta, SWT.NONE, this.wStepname.getText(), progressDialog.getPreviewRowsMeta(this.wStepname.getText()), progressDialog.getPreviewRows(this.wStepname.getText()),
						loggingText);
					prd.open();
				}
			}

		}
	}
}
