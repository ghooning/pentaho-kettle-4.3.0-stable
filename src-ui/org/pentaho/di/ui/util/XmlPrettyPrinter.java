package org.pentaho.di.ui.util;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

public class XmlPrettyPrinter
{
	DocumentBuilderFactory documentBuilderFactory = null;

	DocumentBuilder documentBuilder = null;

	Document document = null;

	DOMImplementationLS DOMiLS = null;

	public XmlPrettyPrinter()
	{
	}

	public String format(String srcXML, String encoding) throws Exception
	{
		try {
			//create a DocumentBuilderFactory object
			this.documentBuilderFactory = DocumentBuilderFactory.newInstance();

			//create a DocumentBuilder object
			this.documentBuilder = this.documentBuilderFactory.newDocumentBuilder();

			//get the DOM tree
			InputSource is = new InputSource(new StringReader(srcXML));
			this.document = this.documentBuilder.parse(is);
		}
		catch (javax.xml.parsers.ParserConfigurationException e) {
			throw new Exception("Unable to save document", e);
		}
		catch (org.xml.sax.SAXException e) {
			throw new Exception("Unable to save document", e);
		}
		catch (java.io.IOException e) {
			throw new Exception("Unable to save document", e);
		}

		//testing the support for DOM Load and Save
		if (this.document.getFeature("Core", "3.0") != null && this.document.getFeature("LS", "3.0") != null) {
			this.DOMiLS = (DOMImplementationLS) this.document.getImplementation().getFeature("LS", "3.0");
			System.out.println("[Using DOM Load and Save]");
		}
		else {
			System.out.println("[DOM Load and Save unsupported]");
			System.exit(0);
		}

		//get a LSSerializer object
		LSSerializer LSS = this.DOMiLS.createLSSerializer();
		LSS.getDomConfig().setParameter("format-pretty-print", new Boolean(true));
		//LSS.getDomConfig().setParameter("xml-declaration", new Boolean(false));

		ByteArrayOutputStream os = new ByteArrayOutputStream();
		LSOutput lsOutput = this.DOMiLS.createLSOutput();
		lsOutput.setEncoding(encoding);
		lsOutput.setByteStream(os);
		LSS.write(this.document, lsOutput);
		os.close();

		return os.toString(encoding);
		//return LSS.writeToString(this.document);
	}

	/*
	public static void main(String[] args)
	{
		XmlPrettyPrinter xmlPrettyPrinter = new XmlPrettyPrinter();
		xmlPrettyPrinter.format("/Volumes/dev/projects/test/test.kjb");
	}
	*/
}
